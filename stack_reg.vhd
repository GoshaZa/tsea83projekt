library ieee;
use ieee.std_logic_1164.all;

entity stack_register is
  generic
    (width  : integer := 16;
     height : integer := 4);
  port
    (clk, rst   : in  std_logic;
     r          : in  std_logic;
     w          : in  std_logic;
     async_data : out std_logic_vector(width-1 downto 0);
     data       : out std_logic_vector(width-1 downto 0);
     datain     : in  std_logic_vector(width-1 downto 0));
end stack_register;

architecture Behavioral of stack_register is
  type stack is array(0 to height - 1) of std_logic_vector(width - 1 downto 0);
  signal current : stack :=
    (others => (others => '0'));
begin

  process (clk, rst) is
  begin
    if rst = '1' then
      current <= (others => (others => '0'));
    elsif rising_edge(clk) then
      if w = '1' then                   -- push
        current(0) <= datain;
        for i in 1 to height-1 loop
          current(i) <= current(i-1);
        end loop;
      elsif r = '1' then                -- pop
        for i in 1 to height-1 loop
          current(i-1) <= current(i);
        end loop;
      end if;
    end if;
  end process;

  data       <= current(0);
  async_data <= current(0);

end Behavioral;
