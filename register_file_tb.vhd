library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity register_file_tb is
end register_file_tb;

architecture behavior of register_file_tb is

  -- Component Declaration for the Unit Under Test (UUT)

  component register_file
    port(
      clk   : in  std_logic;
      w     : in  std_logic;
      q     : out std_logic_vector(15 downto 0);
      qin   : in  std_logic_vector(15 downto 0);
      reset : in  std_logic;
      grx   : in  std_logic_vector(1 downto 0)
      );
  end component;


  --Inputs
  signal clk   : std_logic                     := '0';
  signal w     : std_logic                     := '0';
  signal qin   : std_logic_vector(15 downto 0) := (others => '0');
  signal reset : std_logic                     := '0';
  signal grx   : std_logic_vector(1 downto 0)  := (others => '0');

  --Outputs
  signal q : std_logic_vector(15 downto 0);

  -- Clock period definitions
  constant clk_period : time := 10 ns;

begin

  -- Instantiate the Unit Under Test (UUT)
  uut : register_file port map (
    clk   => clk,
    w     => w,
    q     => q,
    qin   => qin,
    reset => reset,
    grx   => grx
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    reset <= '1';
    wait for 100 ns;
    reset <= '0';
    qin   <= "0000000000000001";
    w     <= '1';
    wait for clk_period;
    qin   <= "0000001100000001";
    grx   <= "01";

    wait for clk_period;
    qin <= "0000000000110011";
    w   <= '0';

    wait for clk_period*10;

    -- insert stimulus here

    wait;
  end process;

end;
