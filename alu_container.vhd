library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity alu_container is
  port (
    clk, rst : in  std_logic;
    styr     : in  std_logic_vector(3 downto 0);
    from_bus : in  std_logic_vector(15 downto 0);
    flags    : out std_logic_vector(3 downto 0);
    data_out : out std_logic_vector(15 downto 0));

end alu_container;

architecture rtl of alu_container is
  component ALU is
    port (clk, rst   : in  std_logic;
          Z, N, C, V : out std_logic;
          a, b       : in  std_logic_vector(15 downto 0);
          styr       : in  std_logic_vector(3 downto 0);
          ut         : out std_logic_vector(15 downto 0));
  end component;

  signal a, b, ut : std_logic_vector(15 downto 0);

begin

  alu_inst : alu
    port map (
      clk  => clk,
      rst  => rst,
      Z    => flags(3),
      N    => flags(2),
      C    => flags(1),
      V    => flags(0),
      a    => a,
      b    => from_bus,
      styr => styr,
      ut   => ut);

  a <= ut;

  data_out <= ut;
end rtl;
