library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.STD_LOGIC_UNSIGNED.all;

use IEEE.numeric_std.all;

use work.cpu_types.all;

entity ppu is
  port (clk, rst : in std_logic;

        ypos : in std_logic_vector (9 downto 0);
        xpos : in std_logic_vector (9 downto 0);

        vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
        vgaBlue          : out std_logic_vector (2 downto 1);

        data_select : in  std_logic_vector(4 downto 0);
        data_we     : in  std_logic;
        data_done   : out std_logic;
        data_in     : in  std_logic_vector(9 downto 0);
        data_out    : out std_logic_vector(9 downto 0));
end ppu;

architecture rtl of ppu is
  component bild_minne
    generic
      (ADDRESS_WIDTH : integer := 17;
       DATA_WIDTH    : integer := 3);
    port
      (clk         : in  std_logic;
       data          : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
       read_addressb : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       read_address  : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       q             : out std_logic_vector(DATA_WIDTH - 1 downto 0);
       qb            : out std_logic_vector(DATA_WIDTH - 1 downto 0));
  end component;

  component sprite_minne is
    generic
      (ADDRESS_WIDTH : integer := 13;
       DATA_WIDTH    : integer := 3);
    port
      (clk         : in  std_logic;
       data          : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
       write_address : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       read_address  : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       we            : in  std_logic;
       q             : out std_logic_vector(DATA_WIDTH - 1 downto 0));
  end component;


  signal pixel : std_logic_vector(2 downto 0);
  signal color : std_logic_vector(7 downto 0);

  signal back_pixel : std_logic_vector(2 downto 0);
  signal read_a     : std_logic_vector(16 downto 0);

  signal back_pixelb : std_logic_vector(2 downto 0);
  signal read_b      : std_logic_vector(16 downto 0);

  signal read_sprite  : std_logic_vector(12 downto 0);
  signal sprite_pixel : std_logic_vector(2 downto 0);
  signal which        : std_logic_vector(1 downto 0) := "00";
begin

  process (clk) is
    variable x1  : integer := 0;
    variable y1  : integer := 0;
    variable rot1 : integer := 3;

    variable x2   : integer := 540;
    variable y2   : integer := 320;
    variable rot2 : integer := 5;

    variable s1   : boolean := false;
    variable sx1  : integer := 0;
    variable sy1  : integer := 0;
    variable ssx1 : integer := 0;
    variable ssy1 : integer := 0;
    variable sr1  : integer := 0;
    variable sd1  : integer := 0;

    variable bmx : integer := 0;
    variable bmy : integer := 0;
    variable bmp : integer := 0;

    variable s2   : boolean := false;
    variable sx2  : integer := 0;
    variable sy2  : integer := 0;
    variable ssx2 : integer := 0;
    variable ssy2 : integer := 0;
    variable sr2  : integer := 0;
    variable sd2  : integer := 0;

    variable sp1, sp2 : integer := 0;
  begin
    if rising_edge(clk) then
      read_a <= std_logic_vector(to_unsigned((320 * conv_integer(ypos(9 downto 1))) + conv_integer(xpos(9 downto 1)), 17));
      read_b <= std_logic_vector(to_unsigned(320 * (bmy/2) + bmx/2, 17));
      pixel  <= back_pixel;
      which  <= "00";

      -- V�lj data att skicka till MMIO
      case data_select is
        when p_p1x => data_out <= std_logic_vector(to_unsigned(x1, 10));
        when p_p1y => data_out <= std_logic_vector(to_unsigned(y1, 10));

        when p_p2x => data_out <= std_logic_vector(to_unsigned(x2, 10));
        when p_p2y => data_out <= std_logic_vector(to_unsigned(y2, 10));

        when p_p1r => data_out <= std_logic_vector(to_unsigned(rot1, 10));
        when p_p2r => data_out <= std_logic_vector(to_unsigned(rot2, 10));

        when p_s1x => data_out <= std_logic_vector(to_unsigned(sx1, 10));
        when p_s1y => data_out <= std_logic_vector(to_unsigned(sy1, 10));

        when p_s2x => data_out <= std_logic_vector(to_unsigned(sx2, 10));
        when p_s2y => data_out <= std_logic_vector(to_unsigned(sy2, 10));

        when p_s1d => data_out <= std_logic_vector(to_unsigned(sd1, 10));
        when p_s2d => data_out <= std_logic_vector(to_unsigned(sd2, 10));

        when p_s1 =>
          if s1 then
            data_out <= "0000000001";
          else
            data_out <= (others => '0');
          end if;
        when p_s2 =>
          if s2 then
            data_out <= "0000000001";
          else
            data_out <= (others => '0');
          end if;
        when p_bmp  => data_out <= "0000000" & back_pixelb;
        when p_sp1  => data_out <= std_logic_vector(to_unsigned(sp1, 10));
        when p_sp2  => data_out <= std_logic_vector(to_unsigned(sp2, 10));
        when others => null;
      end case;

      data_done <= '0'; -- S�tts till '1' n�r write �r klar

      -- MMIO s�ger att CPU vill skriva
      if data_we = '1' then
        data_done <= '1';
        case data_select is
          when p_p1x => x1 := conv_integer(data_in);
          when p_p1y => y1 := conv_integer(data_in);

          when p_p2x => x2 := conv_integer(data_in);
          when p_p2y => y2 := conv_integer(data_in);

          when p_p1r => rot1 := conv_integer(data_in(2 downto 0));  -- 0 -> 7
          when p_p2r => rot2 := conv_integer(data_in(2 downto 0));  -- 0 -> 7

          when p_s1x => sx1 := conv_integer(data_in);
          when p_s1y => sy1 := conv_integer(data_in);

          when p_s2x => sx2 := conv_integer(data_in);
          when p_s2y => sy2 := conv_integer(data_in);

          when p_s1d => sd1 := conv_integer(data_in);
          when p_s2d => sd2 := conv_integer(data_in);

          when p_s1 =>
            if data_in(0) = '1' then
              s1 := true;
            else
              s1 := false;
            end if;
          when p_s2 =>
            if data_in(0) = '1' then
              s2 := true;
            else
              s2 := false;
            end if;
          when p_bmx  => bmx := conv_integer(data_in);
          when p_bmy  => bmy := conv_integer(data_in);
          when others => null;
        end case;
      end if;

      if s1 then
        if sd1 = 0 then
          sr1  := rot1;
          ssx1 := x1 + 16;
          ssy1 := y1 + 16;
          sd1  := 30;                   -- fulhack
        end if;
        case sr1 is
          when 0 =>                     -- upp
            sx1 := ssx1;
            sy1 := ssy1 - sd1;
          when 1 =>                     -- upp h�ger
            sx1 := ssx1 + ((sd1 * 11) / 16);
            sy1 := ssy1 - ((sd1 * 11) / 16);
          when 2 =>                     -- h�ger
            sx1 := ssx1 + sd1;
            sy1 := ssy1;
          when 3 =>                     -- ner h�ger
            sx1 := ssx1 + ((sd1 * 11) / 16);
            sy1 := ssy1 + ((sd1 * 11) / 16);
          when 4 =>                     -- ner
            sx1 := ssx1;
            sy1 := ssy1 + sd1;
          when 5 =>                     -- ner v�nster
            sx1 := ssx1 - ((sd1 * 11) / 16);
            sy1 := ssy1 + ((sd1 * 11) / 16);
          when 6 =>                     -- v�nster
            sx1 := ssx1 - sd1;
            sy1 := ssy1;
          when 7 =>                     -- upp v�nster
            sx1 := ssx1 - ((sd1 * 11) / 16);
            sy1 := ssy1 - ((sd1 * 11) / 16);
          when others => null;
        end case;
      else
        sd1 := 0;
      end if;

      if s2 then
        if sd2 = 0 then
          sr2  := rot2;
          ssx2 := x2 + 16;
          ssy2 := y2 + 16;
          sd2  := 30;                   -- fulhack
        end if;
        case sr2 is
          when 0 =>                     -- upp
            sx2 := ssx2;
            sy2 := ssy2 - sd2;
          when 1 =>                     -- upp h�ger
            sx2 := ssx2 + ((sd2 * 11) / 16);
            sy2 := ssy2 - ((sd2 * 11) / 16);
          when 2 =>                     -- h�ger
            sx2 := ssx2 + sd2;
            sy2 := ssy2;
          when 3 =>                     -- ner h�ger
            sx2 := ssx2 + ((sd2 * 11) / 16);
            sy2 := ssy2 + ((sd2 * 11) / 16);
          when 4 =>                     -- ner
            sx2 := ssx2;
            sy2 := ssy2 + sd2;
          when 5 =>                     -- ner v�nster
            sx2 := ssx2 - ((sd2 * 11) / 16);
            sy2 := ssy2 + ((sd2 * 11) / 16);
          when 6 =>                     -- v�nster
            sx2 := ssx2 - sd2;
            sy2 := ssy2;
          when 7 =>                     -- upp v�nster
            sx2 := ssx2 - ((sd2 * 11) / 16);
            sy2 := ssy2 - ((sd2 * 11) / 16);
          when others => null;
        end case;
      else
        sd2 := 0;
      end if;

      if xpos = sx1 and ypos = sy1 then
        if pixel = "000" then
          sp1 := 0;
        else
          sp1 := 2;
        end if;
      end if;

      if xpos = sx2 and ypos = sy2 then
        if pixel = "000" then
          sp2 := 0;
        else
          sp2 := 2;
        end if;
      end if;


      if xpos >= x1 and ypos >= y1 and xpos < x1 + 32 and ypos < y1 + 32 then
        read_sprite <= std_logic_vector(to_unsigned(32*32*rot1+(32 * (conv_integer(ypos) - y1)) + conv_integer(xpos) - x1, 13));
        if sprite_pixel /= "000" then
          pixel <= sprite_pixel;
          which <= "01";
          if xpos = sx2 and ypos = sy2 then
            if s2 then
              sp2 := 1;
            else
              sp2 := 0;
            end if;
          end if;
        end if;
      elsif xpos >= x2 and ypos >= y2 and xpos < x2 + 32 and ypos < y2 + 32 then
        read_sprite <= std_logic_vector(to_unsigned(32*32*rot2+(32 * (conv_integer(ypos) - y2)) + conv_integer(xpos) - x2, 13));
        if sprite_pixel /= "000" then
          pixel <= sprite_pixel;
          which <= "10";
          if xpos = sx1 and ypos = sy1 then
            if s1 then
              sp1 := 1;
            else
              sp1 := 0;
            end if;
          end if;
        end if;
      elsif s1 and xpos >= sx1 and ypos >= sy1 and xpos < sx1 + 3 and ypos < sy1 + 3 then
        which <= "11";
      elsif s2 and xpos >= sx2 and ypos >= sy2 and xpos < sx2 + 3 and ypos < sy2 + 3 then
        which <= "11";
      end if;

      case which is
        when "00" =>
          case pixel is
            when "000"  => color <= "11111111";  --01001001
            when "001"  => color <= "10100101";
            when "010"  => color <= "01100000";
            when "011"  => color <= "00000000";
            when "100"  => color <= "01001001";
            when "101"  => color <= "10010010";
            when "110"  => color <= "01101110";
            when "111"  => color <= "11111100";
            when others => color <= "00000000";
          end case;
        when "11" =>
          color <= "00000000";
        when others =>
          case pixel is
            when "001"  => color <= "00000000";
            when "010"  => color <= "01101101";
            when "011"  => color <= "11110110";
            when "100"  => color <= "11110111";
            when "101"  => color <= "01100000";
            when "110"  => color <= "11010010";
            when "111"  => color <= "00000000";
            when "000"  => color <= "11111111";
            when others => color <= "00011100";
          end case;
      end case;
    end if;
  end process;

  --with pixel select
  --  color <=
  --  "10010010" when "000",
  --  "11111111" when "001",
  --  "10000111" when others;

  vgaRed   <= color(7 downto 5);
  vgaGreen <= color(4 downto 2);
  vgaBlue  <= color(1 downto 0);

  bm : bild_minne
    generic map (ADDRESS_WIDTH => 17, DATA_WIDTH => 3)
    port map (
      clk         => clk,
      read_address  => read_a,
      read_addressb => read_b,
      q             => back_pixel,
      qb            => back_pixelb,
      data          => "000");

  sm : sprite_minne
    generic map (ADDRESS_WIDTH => 13, DATA_WIDTH => 3)
    port map (
      clk         => clk,
      read_address  => read_sprite,
      write_address => "0000000000000",
      q             => sprite_pixel,
      data          => "000",
      we            => '0');

end architecture rtl;
