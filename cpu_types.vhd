library IEEE;
use IEEE.STD_LOGIC_1164.all;

package cpu_types is
  constant alu_none       : std_logic_vector(3 downto 0) := "0000";
  constant alu_add        : std_logic_vector(3 downto 0) := "0001";
  constant alu_sub        : std_logic_vector(3 downto 0) := "0010";
  constant alu_and        : std_logic_vector(3 downto 0) := "0011";
  constant alu_or         : std_logic_vector(3 downto 0) := "0100";
  constant alu_lsl        : std_logic_vector(3 downto 0) := "0101";
  constant alu_lsr        : std_logic_vector(3 downto 0) := "0110";
  constant alu_fb         : std_logic_vector(3 downto 0) := "0111";  -- from bus
  constant alu_not        : std_logic_vector(3 downto 0) := "1000";
  constant alu_nand       : std_logic_vector(3 downto 0) := "1001";
  constant alu_nor        : std_logic_vector(3 downto 0) := "1010";
  constant alu_add_noflag : std_logic_vector(3 downto 0) := "1011";

  constant fb_nop : std_logic_vector(2 downto 0) := "000";
  constant fb_ir  : std_logic_vector(2 downto 0) := "001";
  constant fb_pm  : std_logic_vector(2 downto 0) := "010";
  constant fb_pc  : std_logic_vector(2 downto 0) := "011";
  constant fb_ar  : std_logic_vector(2 downto 0) := "100";
  constant fb_hr  : std_logic_vector(2 downto 0) := "101";
  constant fb_gr  : std_logic_vector(2 downto 0) := "110";
  constant fb_asr : std_logic_vector(2 downto 0) := "111";


  constant tb_nop : std_logic_vector(2 downto 0) := "000";
  constant tb_ir  : std_logic_vector(2 downto 0) := "001";
  constant tb_pm  : std_logic_vector(2 downto 0) := "010";
  constant tb_pc  : std_logic_vector(2 downto 0) := "011";
  constant tb_ar  : std_logic_vector(2 downto 0) := "100";
  constant tb_hr  : std_logic_vector(2 downto 0) := "101";
  constant tb_gr  : std_logic_vector(2 downto 0) := "110";
  constant tb_asr : std_logic_vector(2 downto 0) := "111";

  constant seq_pcpp  : std_logic_vector(3 downto 0)  := "0000";  -- std
  constant seq_pck1  : std_logic_vector(3 downto 0)  := "0001";
  constant seq_pck2  : std_logic_vector(3 downto 0)  := "0010";
  constant seq_pc0   : std_logic_vector(3 downto 0)  := "0011";
  constant seq_pcadr : std_logic_vector(3 downto 0)  := "0101";
  constant seq_pcjsr : std_logic_vector(3 downto 0)  := "0110";
  constant seq_pcrts : std_logic_vector(3 downto 0)  := "0111";
  constant seq_jnz   : std_logic_vector(3 downto 0)  := "1001";  -- Z = 0 Jumps
  constant seq_jz    : std_logic_vector(3 downto 0)  := "1010";  -- Z = 1
  constant seq_jc    : std_logic_vector(3 downto 0)  := "1011";  -- C = 1
  constant seq_jv    : std_logic_vector(3 downto 0)  := "1100";  -- V = 1
  constant seq_jn    : std_logic_vector(3 downto 0)  := "1101";  -- N = 1
  constant seq_jnn   : std_logic_vector(3 downto 0)  := "1110";  -- N = 0
  constant seq_halt  : std_logic_vector(3 downto 0)  := "1111";  --

  constant p_p1x : std_logic_vector(4 downto 0) := "00000";
  constant p_p1y : std_logic_vector(4 downto 0) := "00001";
  constant p_p2x : std_logic_vector(4 downto 0) := "00010";
  constant p_p2y : std_logic_vector(4 downto 0) := "00011";

  constant p_p1r : std_logic_vector(4 downto 0) := "00100";
  constant p_p2r : std_logic_vector(4 downto 0) := "00101";

  constant p_s1x : std_logic_vector(4 downto 0) := "00110";
  constant p_s1y : std_logic_vector(4 downto 0) := "00111";
  constant p_s2x : std_logic_vector(4 downto 0) := "01000";
  constant p_s2y : std_logic_vector(4 downto 0) := "01001";

  constant p_s1d : std_logic_vector(4 downto 0) := "01010";
  constant p_s2d : std_logic_vector(4 downto 0) := "01011";

  constant p_s1 : std_logic_vector(4 downto 0) := "01100";
  constant p_s2 : std_logic_vector(4 downto 0) := "01101";

  constant p_bmx : std_logic_vector(4 downto 0) := "01110";
  constant p_bmy : std_logic_vector(4 downto 0) := "01111";
  constant p_bmp : std_logic_vector(4 downto 0) := "10000";

  constant p_sp1 : std_logic_vector(4 downto 0) := "10001";
  constant p_sp2 : std_logic_vector(4 downto 0) := "10010";

end package;

