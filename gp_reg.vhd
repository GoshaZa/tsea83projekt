library ieee;
use ieee.std_logic_1164.all;

entity general_purpose_register is
  generic
    (width : integer := 16);
  port
    (clk, rst   : in  std_logic;
     w          : in  std_logic;
     async_data : out std_logic_vector(width-1 downto 0);
     data       : out std_logic_vector(width-1 downto 0);
     datain     : in  std_logic_vector(width-1 downto 0));
end general_purpose_register;

architecture Behavioral of general_purpose_register is
  signal current : std_logic_vector(width - 1 downto 0) := (others => '0');
begin

  process (clk, rst) is
  begin
    if rst = '1' then
      current <= (others => '0');
    elsif rising_edge(clk) then
      if w = '1' then
        current <= datain;
      end if;
    end if;
  end process;

  data       <= current;
  async_data <= current;
  
end Behavioral;
