---------------------------------------------------------------------------------------
-- Company: Digilent Inc.
-- Engineer: Josh Sackos
--
-- Create Date:    07/11/2012
-- Module Name:    PmodJSTK_Demo
-- Project Name:         PmodJSTK_Demo
-- Target Devices: Nexys3
-- Tool versions:  ISE 14.1
-- Description: This is a demo for the Digilent PmodJSTK. Data is sent and received
--                                       to and from the PmodJSTK at a frequency of 1kHz, and positional
--                                       data is displayed on the seven segment display (SSD). The positional
--                                       data of the joystick ranges from 0 to 1023 in both the X and Y
--                                       directions. Only one coordinate can be displayed on the SSD at a
--                                       time, therefore switch SW0 is used to select which coordinate's data
--                               to display. Postional data displayed on the SSD will be updated at a
--                                       frequency of 5Hz. The status of the buttons on the PmodJSTK are
--                                       displayed on LD2, LD1, and LD0 on the Nexys3. The LEDs will
--                                       illuminate when a button is pressed. Switches SW2 adn SW1 on the
--                                       Nexys3 will turn on LD1 and LD2 on the PmodJSTK respectively. Button
--                                       BTND on the Nexys3 is used for reseting the demo. The PmodJSTK
--                                       connects to pins [4:1] on port JA on the Nexys3. SPI mode 0 is used
--                                       for communication between the PmodJSTK and the Nexys3.
--
--                                       NOTE: The digits on the SSD may at times appear to flicker, this
--                                                  is due to small pertebations in the positional data being read
--                                                       by the PmodJSTK's ADC. To reduce the flicker simply reduce
--                                                       the rate at which the data being displayed is updated.
--
-- Revision History:
--                                              Revision 0.01 - File Created (Josh Sackos)
---------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.all;


--  ===================================================================================
--                                                              Define Module, Inputs and Outputs
--  ===================================================================================
entity PmodJSTK_Demo is
  port (CLK      : in  std_logic;       -- 100Mhz onboard clock
        RST      : in  std_logic;       -- Button D
        MISO     : in  std_logic;       -- Master In Slave Out, JA3
        SW       : in  std_logic_vector (2 downto 0);  -- Switches 2, 1, and 0
        SS       : out std_logic;       -- Slave Select, Pin 1, Port JA
        MOSI     : out std_logic;       -- Master Out Slave In, Pin 2, Port JA
        SCLK     : out std_logic;       -- Serial Clock, Pin 4, Port JA
        LED      : out std_logic_vector (2 downto 0);  -- LEDs 2, 1, and 0
        posDataX : out std_logic_vector(9 downto 0);
        posDataY : out std_logic_vector(9 downto 0));
end PmodJSTK_Demo;

architecture Behavioral of PmodJSTK_Demo is

--  ===================================================================================
--                                                                                              Components
--  ===================================================================================

  -- **********************************************
  --                                    SPI Interface
  -- **********************************************
  component PmodJSTK

    port (CLK    : in    std_logic;
          RST    : in    std_logic;
          sndRec : in    std_logic;
          DIN    : in    std_logic_vector (7 downto 0);
          MISO   : in    std_logic;
          SS     : out   std_logic;
          SCLK   : out   std_logic;
          MOSI   : out   std_logic;
          DOUT   : inout std_logic_vector (39 downto 0)

          );

  end component;

  -- **********************************************
  --                            5Hz Clock Divider
  -- **********************************************
  component ClkDiv_5Hz

    port (CLK    : in    std_logic;
          RST    : in    std_logic;
          CLKOUT : inout std_logic
          );

  end component;

--  ===================================================================================
--                                                                              Signals and Constants
--  ===================================================================================

  -- Holds data to be sent to PmodJSTK
  signal sndData : std_logic_vector(7 downto 0) := X"00";

  -- Signal to send/receive data to/from PmodJSTK
  signal sndRec : std_logic;

  -- Signal indicating that SPI interface is busy
  signal BUSY : std_logic := '0';

  -- Data read from PmodJSTK
  signal jstkData : std_logic_vector(39 downto 0) := (others => '0');

--  ===================================================================================
--                                                                                      Implementation
--  ===================================================================================
begin

  -------------------------------------------------
  --                            PmodJSTK Interface
  ------------------------------------------------
  PmodJSTK_Int : PmodJSTK port map(
    CLK    => CLK,
    RST    => RST,
    sndRec => sndRec,
    DIN    => sndData,
    MISO   => MISO,
    SS     => SS,
    SCLK   => SCLK,
    MOSI   => MOSI,
    DOUT   => jstkData
    );

  -------------------------------------------------
  --             Send Receive Signal Generator
  -------------------------------------------------
  genSndRec : ClkDiv_5Hz port map(
    CLK    => CLK,
    RST    => RST,
    CLKOUT => sndRec
    );

  posDataX <= (jstkData(25 downto 24) & jstkData(39 downto 32));
  posDataY <= (jstkData(9 downto 8) & jstkData(23 downto 16));

  -- Data to be sent to PmodJSTK, lower two bits will turn on leds on PmodJSTK
  sndData <= "100000" & SW(1) & SW(2);

  -- Assign PmodJSTK button status to LED[2:0]
  process(sndRec, RST)
  begin
    if(RST = '1') then
      LED <= "000";
    elsif rising_edge(sndRec) then
      LED <= jstkData(1) & jstkData(2) & jstkData(0);
    end if;
  end process;

end Behavioral;
