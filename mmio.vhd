library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.cpu_types.all;

entity mmio is
  port (
    clk, rst     : in  std_logic;
    cpu_r, cpu_w : in  std_logic;
    cpu_adr      : in  std_logic_vector(15 downto 0);
    cpu_data     : in  std_logic_vector(15 downto 0);
    cpu_datain   : out std_logic_vector(15 downto 0);
    cpu_wait     : in  std_logic;
    cpu_done     : out std_logic;

    ppu_we     : out std_logic;
    ppu_in     : in  std_logic_vector(9 downto 0);
    ppu_out    : out std_logic_vector(9 downto 0);
    ppu_select : out std_logic_vector(4 downto 0);
    ppu_done   : in  std_logic;

    ram_we     : out std_logic;
    ram_adr    : out std_logic_vector(7 downto 0);
    ram_data   : in  std_logic_vector(15 downto 0);
    ram_datain : out std_logic_vector(15 downto 0);

    joy1x : in std_logic_vector(9 downto 0);
    joy1y : in std_logic_vector(9 downto 0);
    joy1k : in std_logic_vector(2 downto 0);
    joy2x : in std_logic_vector(9 downto 0);
    joy2y : in std_logic_vector(9 downto 0);
    joy2k : in std_logic_vector(2 downto 0);

    ledout : out std_logic_vector(15 downto 0);

    lamp : out std_logic_vector(7 downto 0)
    );
end mmio;

architecture Behavioral of mmio is
  signal flipy, flipy2 : std_logic_vector(2 downto 0);

  signal ledright, ledleft : std_logic_vector(3 downto 0);

  signal wait_done : std_logic := '0';
begin

  ram_adr    <= cpu_adr(7 downto 0);
  ram_datain <= cpu_data;
  ppu_out    <= cpu_data(9 downto 0);

  ledout <= ledleft & "11111111" & ledright;

  process (cpu_adr, cpu_w, wait_done, ppu_done)
  begin
    if cpu_adr < 224 then  --                      00 <--> DF
      cpu_done <= wait_done;
      cpu_done <= '1';
      ram_we   <= cpu_w;
      ppu_we   <= '0';
    elsif cpu_adr > 223 and cpu_adr < 243 then  -- E0 <--> F2
      cpu_done <= wait_done;
      ram_we   <= '0';
      ppu_we   <= cpu_w;
    else  --                                       F3 <--> FF
      cpu_done <= wait_done;
      ram_we   <= cpu_w;
      ppu_we   <= '0';
    end if;
  end process;

  -- Vänta en klockpuls i icke-done till CPUn när den läser från PPUn
  process (clk) is
  begin
    if rising_edge(clk) then
      if cpu_wait = '1' and cpu_adr > 223 then
        wait_done <= '1';
      else
        wait_done <= '0';
      end if;

      if cpu_adr = 253 and cpu_w = '1' then             --         FD
        ledleft <= cpu_data(3 downto 0);
      end if;

      if cpu_adr = 254 and cpu_w = '1' then             --         FE
        ledright <= cpu_data(3 downto 0);
      end if;

    end if;
  end process;

  with cpu_adr select
    ppu_select <=
    p_sp1   when X"00E0",
    p_sp2   when X"00E1",
    p_p1x   when X"00E2",
    p_p1y   when X"00E3",
    p_p2x   when X"00E4",
    p_p2y   when X"00E5",
    p_p1r   when X"00E6",
    p_p2r   when X"00E7",
    p_s1x   when X"00E8",
    p_s1y   when X"00E9",
    p_s2x   when X"00Ea",
    p_s2y   when X"00Eb",
    p_s1d   when X"00Ec",
    p_s2d   when X"00Ed",
    p_s1    when X"00Ee",
    p_s2    when X"00Ef",
    p_bmx   when X"00F0",
    p_bmy   when X"00F1",
    p_bmp   when X"00F2",
    "00000" when others;

  flipy  <= 7 - joy1y(9 downto 7);
  flipy2 <= 7 - joy2y(9 downto 7);

  with cpu_adr select
    cpu_datain <=
    "000000" & ppu_in                   when X"00E0",
    "000000" & ppu_in                   when X"00E1",
    "000000" & ppu_in                   when X"00E2",
    "000000" & ppu_in                   when X"00E3",
    "000000" & ppu_in                   when X"00E4",
    "000000" & ppu_in                   when X"00E5",
    "000000" & ppu_in                   when X"00E6",
    "000000" & ppu_in                   when X"00E7",
    "000000" & ppu_in                   when X"00E8",
    "000000" & ppu_in                   when X"00E9",
    "000000" & ppu_in                   when X"00EA",
    "000000" & ppu_in                   when X"00EB",
    "000000" & ppu_in                   when X"00EC",
    "000000" & ppu_in                   when X"00ED",
    "000000" & ppu_in                   when X"00EE",
    "000000" & ppu_in                   when X"00EF",
    "000000" & ppu_in                   when X"00F0",
    "000000" & ppu_in                   when X"00F1",
    "000000" & ppu_in                   when X"00F2",
    "0000000000000" & joy1x(9 downto 7) when X"00F3",
    "0000000000000" & flipy             when X"00F4",
    "0000000000000" & joy2x(9 downto 7) when X"00F5",
    "0000000000000" & flipy2            when X"00F6",

    "000000000000000" & joy1k(0) when X"00F7",
    "000000000000000" & joy1k(1) when X"00F8",
    "000000000000000" & joy1k(2) when X"00F9",
    "000000000000000" & joy2k(0) when X"00FA",
    "000000000000000" & joy2k(1) when X"00FB",
    "000000000000000" & joy2k(2) when X"00FC",
    "000000000000" & ledleft     when X"00FD",
    "000000000000" & ledright    when X"00FE",
    ram_data                     when others;


  lamp <= joy1y(9 downto 2);

end Behavioral;
