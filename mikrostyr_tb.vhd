library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

use work.cpu_types.all;

entity mikrostyr_tb is
end mikrostyr_tb;

architecture behavior of mikrostyr_tb is

  -- Component Declaration for the Unit Under Test (UUT)
  
  component mikrostyr
    port(
      clk     : in  std_logic;
      rst     : in  std_logic;
      address : out std_logic_vector(5 downto 0);
      seq     : in  std_logic_vector(3 downto 0);
      madr    : in  std_logic_vector(5 downto 0);
      alu_f   : in  std_logic_vector(3 downto 0);
      ir      : in  std_logic_vector(15 downto 0)
      );
  end component;

  component mikrominne is
    generic
      (ADDRESS_WIDTH : integer := 6;
       DATA_WIDTH    : integer := 24);
    port
      (clock        : in  std_logic;
       read_address : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       data         : out std_logic_vector(DATA_WIDTH - 1 downto 0));
  end component;

  signal clk   : std_logic                     := '0';
  signal rst   : std_logic                     := '0';

  --Outputs
  signal address : std_logic_vector(5 downto 0);
  signal data    : std_logic_vector(23 downto 0);

  --Inputs
  alias seq    : std_logic_vector(3 downto 0) is data(9 downto 6);
  alias madr   : std_logic_vector(5 downto 0) is data(5 downto 0);
  signal alu_f : std_logic_vector(3 downto 0)  := (others => '0');
  signal ir    : std_logic_vector(15 downto 0) := (others => '0');

  alias alu_do : std_logic_vector(3 downto 0) is data(23 downto 20);
  alias tb : std_logic_vector(2 downto 0) is data(19 downto 17);
  alias fb : std_logic_vector(2 downto 0) is data(16 downto 14);
  -- Clock period definitions
  constant clk_period : time := 10 ns;
  
begin

  -- Instantiate the Unit Under Test (UUT)
  uut : mikrostyr port map (
    clk     => clk,
    rst     => rst,
    address => address,
    seq     => seq,
    madr    => madr,
    alu_f   => alu_f,
    ir      => ir
    );

  mminne : mikrominne
    generic map (ADDRESS_WIDTH => 6, DATA_WIDTH => 24)
    port map (
      clock        => clk,
      read_address => address,
      data         => data);

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    rst <= '1';
    wait for 100 ns;
	 wait until rising_edge(clk);
    rst <= '0';
	 wait until fb = fb_ir;
	 ir <= "0001" & "00" & "00" & "00000000"; -- ???
	 wait until fb = fb_ir;
	 ir <= "0000" & "00" & "00" & "00000000"; -- Halt

    wait for clk_period*10;

    wait;
  end process;

end;
