library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity top_tb is
end top_tb;

architecture behavior of top_tb is

  -- Component Declaration for the Unit Under Test (UUT)

  component top
    port(
      clk      : in  std_logic;
      rst      : in  std_logic;
      btnd     : in  std_logic;
      btnr     : in  std_logic;
      hsynch   : out std_logic;
      vsynch   : out std_logic;
      vgaRed   : out std_logic_vector(2 downto 0);
      vgaGreen : out std_logic_vector(2 downto 0);
      vgaBlue  : out std_logic_vector(2 downto 1);
      MISO     : in  std_logic;
      MISO2    : in  std_logic;
      SW       : in  std_logic_vector(2 downto 0);
      SS       : out std_logic;
      SS2      : out std_logic;
      MOSI     : out std_logic;
      MOSI2    : out std_logic;
      SCLK     : out std_logic;
      SCLK2    : out std_logic
      );
  end component;


  --Inputs
  signal clk   : std_logic                    := '0';
  signal rst   : std_logic                    := '0';
  signal btnd  : std_logic                    := '0';
  signal btnr  : std_logic                    := '0';
  signal MISO  : std_logic                    := '0';
  signal MISO2 : std_logic                    := '0';
  signal SW    : std_logic_vector(2 downto 0) := (others => '0');

  --Outputs
  signal hsynch   : std_logic;
  signal vsynch   : std_logic;
  signal vgaRed   : std_logic_vector(2 downto 0);
  signal vgaGreen : std_logic_vector(2 downto 0);
  signal vgaBlue  : std_logic_vector(2 downto 1);
  signal SS       : std_logic;
  signal SS2      : std_logic;
  signal MOSI     : std_logic;
  signal MOSI2    : std_logic;
  signal SCLK     : std_logic;
  signal SCLK2    : std_logic;

  -- Clock period definitions
  constant clk_period  : time := 10 ns;
  constant SCLK_period : time := 10 ns;

begin

  -- Instantiate the Unit Under Test (UUT)
  uut : top port map (
    clk      => clk,
    rst      => rst,
    btnd     => btnd,
    btnr     => btnr,
    hsynch   => hsynch,
    vsynch   => vsynch,
    vgaRed   => vgaRed,
    vgaGreen => vgaGreen,
    vgaBlue  => vgaBlue,
    MISO     => MISO,
    MISO2    => MISO2,
    SW       => SW,
    SS       => SS,
    SS2      => SS2,
    MOSI     => MOSI,
    MOSI2    => MOSI2,
    SCLK     => SCLK,
    SCLK2    => SCLK2
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  SCLK_process : process
  begin
    SCLK <= '0';
    wait for SCLK_period/2;
    SCLK <= '1';
    wait for SCLK_period/2;
  end process;

  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    rst <= '1';
    wait for 95 ns;
    rst <= '0';

    wait for clk_period*10;

    -- insert stimulus here

    wait;
  end process;

end;
