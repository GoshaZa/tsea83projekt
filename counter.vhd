library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.NUMERIC_STD.all;

entity counter is
  port (clk   : in  std_logic;
        q     : out std_logic_vector(15 downto 0);
        qin   : in  std_logic_vector(15 downto 0);
        inc   : in  std_logic;
        reset : in  std_logic;
        w     : in  std_logic);
end counter;

architecture Behavioral of counter is
  signal current : std_logic_vector(15 downto 0) := "0000000000000000";
begin

  process(clk, reset) is
  begin
    if reset = '1' then
      current <= "0000000000000000";
    elsif rising_edge(clk) then
      if inc = '1' then
        current <= current + 1;
      end if;

      if w = '1' then
        current <= qin;
      end if;

    end if;
  end process;

  q <= current;

end Behavioral;
