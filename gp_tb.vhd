
library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity gp_tb is
end gp_tb;

architecture behavior of gp_tb is

  -- Component Declaration for the Unit Under Test (UUT)

  component general_purpose_register
    port(
      clk        : in  std_logic;
      rst        : in  std_logic;
      w          : in  std_logic;
      async_data : out std_logic_vector(15 downto 0);
      data       : out std_logic_vector(15 downto 0);
      datain     : in  std_logic_vector(15 downto 0)
      );
  end component;


  --Inputs
  signal clk    : std_logic                     := '0';
  signal rst    : std_logic                     := '0';
  signal w      : std_logic                     := '0';
  signal datain : std_logic_vector(15 downto 0) := (others => '0');

  --Outputs
  signal async_data : std_logic_vector(15 downto 0);
  signal data       : std_logic_vector(15 downto 0);

  -- Clock period definitions
  constant clk_period : time := 10 ns;

begin

  -- Instantiate the Unit Under Test (UUT)
  uut : general_purpose_register port map (
    clk        => clk,
    rst        => rst,
    w          => w,
    async_data => async_data,
    data       => data,
    datain     => datain
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    rst    <= '1';
    datain <= "0101010101010101";
    wait for 95 ns;
    rst    <= '0';
    w      <= '1';
    wait for clk_period;
    datain <= "0000000000000000";
    w      <= '0';


    wait for clk_period*10;

    -- insert stimulus here

    wait;
  end process;

end;
