--////////////////////////////////////////////////////////////////////////////////
-- Company: Digilent Inc.
-- Engineer: Josh Sackos
--
-- Create Date:    07/11/2012
-- Module Name:    ClkDiv_5Hz
-- Project Name:         PmodJSTK_Demo
-- Target Devices: Nexys3
-- Tool versions:  ISE 14.1
-- Description: Converts input 100MHz clock signal to a 5Hz clock signal.
--
-- Revision History:
--                                              Revision 0.01 - File Created (Josh Sackos)
--////////////////////////////////////////////////////////////////////////////////
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- ====================================================================================
--                                                                                Define Module
-- ====================================================================================
entity ClkDiv_5Hz is
  port (CLK    : in    std_logic;       -- 100MHz onboard clock
        RST    : in    std_logic;       -- Reset
        CLKOUT : inout std_logic);      -- New clock output
end ClkDiv_5Hz;

architecture Behavioral of ClkDiv_5Hz is

-- ====================================================================================
--                                                             Signals and Constants
-- ====================================================================================

  -- Current count value
  signal   clkCount  : std_logic_vector(23 downto 0) := (others => '0');
  -- Value to toggle output clock at
  constant cntEndVal : std_logic_vector(23 downto 0) := X"189680";

--  ===================================================================================
--                                                                                      Implementation
--  ===================================================================================
begin

  -------------------------------------------------
  --    5Hz Clock Divider Generates Send/Receive signal
  -------------------------------------------------
  process(CLK, RST)
  begin

                                        -- Reset clock
    if(RST = '1') then
      CLKOUT   <= '0';
      clkCount <= X"000000";
    elsif rising_edge(CLK) then
      if(clkCount = cntEndVal) then
        CLKOUT   <= not CLKOUT;
        clkCount <= X"000000";
      else
        clkCount <= clkCount + '1';
      end if;
    end if;

  end process;

end Behavioral;
