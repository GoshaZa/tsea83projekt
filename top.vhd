library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cpu_types.all;

entity top is
  port (
    lamp : out std_logic_vector(7 downto 0);
    --  Standardmoj
    clk, rst   : in std_logic;
    btnd, btnr : in std_logic;

    -- VGA
    hsynch, vsynch   : out std_logic;
    vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
    vgaBlue          : out std_logic_vector (2 downto 1);

    -- joystick
    MISO, MISO2 : in  std_logic;        -- Master In Slave Out, JA3
    SW          : in  std_logic_vector (2 downto 0);  -- Switches 2, 1, and 0
    SS, SS2     : out std_logic;        -- Slave Select, Pin 1, Port JA
    MOSI, MOSI2 : out std_logic;        -- Master Out Slave In, Pin 2, Port JA
    SCLK, SCLK2 : out std_logic;        -- Serial Clock, Pin 4, Port JA

    -- LED-display
    ca, cb, cc, cd, ce, cf, cg, dp : out std_logic;
    an                             : out std_logic_vector (3 downto 0)
    );
end top;

architecture rtl of top is
  component cpu
    port
      (clk, rst  : in  std_logic;
       mem_wait  : out std_logic;
       mem_write : out std_logic;
       mem_done  : in  std_logic;
       pm_adr    : out std_logic_vector(16 - 1 downto 0);
       pm_data   : out std_logic_vector(16 - 1 downto 0);
       pm_datain : in  std_logic_vector(16 - 1 downto 0));
  end component;

  component mmio
    port (
      clk, rst     : in  std_logic;
      cpu_r, cpu_w : in  std_logic;
      cpu_adr      : in  std_logic_vector(15 downto 0);
      cpu_data     : in  std_logic_vector(15 downto 0);
      cpu_datain   : out std_logic_vector(15 downto 0);
      cpu_wait     : in  std_logic;
      cpu_done     : out std_logic;

      ppu_we     : out std_logic;
      ppu_select : out std_logic_vector(4 downto 0);
      ppu_done   : in  std_logic;
      ppu_in     : in  std_logic_vector(9 downto 0);
      ppu_out    : out std_logic_vector(9 downto 0);

      ram_we     : out std_logic;
      ram_adr    : out std_logic_vector(7 downto 0);
      ram_data   : in  std_logic_vector(15 downto 0);
      ram_datain : out std_logic_vector(15 downto 0);

      joy1x : in std_logic_vector(9 downto 0);
      joy1y : in std_logic_vector(9 downto 0);
      joy1k : in std_logic_vector(2 downto 0);
      joy2x : in std_logic_vector(9 downto 0);
      joy2y : in std_logic_vector(9 downto 0);
      joy2k : in std_logic_vector(2 downto 0);

      ledout : out std_logic_vector(15 downto 0);

      lamp : out std_logic_vector(7 downto 0)
      );
  end component;

  component ram
    generic
      (ADDRESS_WIDTH : integer := 8;
       DATA_WIDTH    : integer := 16);
    port
      (clk           : in  std_logic;
       data          : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
       write_address : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       read_address  : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       we            : in  std_logic;
       q             : out std_logic_vector(DATA_WIDTH - 1 downto 0));
  end component;

  component PmodJSTK_Demo
    port (CLK      : in  std_logic;     -- 100Mhz onboard clock
          RST      : in  std_logic;     -- Button D
          MISO     : in  std_logic;     -- Master In Slave Out, JA3
          SW       : in  std_logic_vector (2 downto 0);  -- Switches 2, 1, and 0
          SS       : out std_logic;     -- Slave Select, Pin 1, Port JA
          MOSI     : out std_logic;     -- Master Out Slave In, Pin 2, Port JA
          SCLK     : out std_logic;     -- Serial Clock, Pin 4, Port JA
          LED      : out std_logic_vector (2 downto 0);  -- LEDs 2, 1, and 0
          posDataX : out std_logic_vector(9 downto 0);
          posDataY : out std_logic_vector(9 downto 0));
  end component;

  component ppu
    port (clk, rst : in std_logic;

          ypos : in std_logic_vector (9 downto 0);
          xpos : in std_logic_vector (9 downto 0);

          vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
          vgaBlue          : out std_logic_vector (2 downto 1);

          data_select : in  std_logic_vector(4 downto 0);
          data_we     : in  std_logic;
          data_done   : out std_logic;
          data_in     : in  std_logic_vector(9 downto 0);
          data_out    : out std_logic_vector(9 downto 0));
  end component;

  component vga_circuit
    port (clk, rst   : in std_logic;
          red, green : in std_logic_vector (2 downto 0);
          blue       : in std_logic_vector (1 downto 0);

          hsynch, vsynch   : out std_logic;
          vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
          vgaBlue          : out std_logic_vector (2 downto 1);
          ypos             : out std_logic_vector (9 downto 0);
          xpos             : out std_logic_vector (9 downto 0);

          -- Test bench
          hcnt_out           : out std_logic_vector (9 downto 0);
          vcnt_out           : out std_logic_vector (9 downto 0);
          video_on_out,
          h_on_out, v_on_out : out std_logic);
  end component;

  component leddriver
    port (clk, rst                       : in  std_logic;
          ca, cb, cc, cd, ce, cf, cg, dp : out std_logic;
          an                             : out std_logic_vector (3 downto 0);
          ledvalue                       : in  std_logic_vector (15 downto 0));
  end component;


  signal cpu_w, ram_we : std_logic;
  signal cpu_wait      : std_logic;
  signal cpu_done      : std_logic;

  signal cpu_adr, cpu_data, cpu_datain,
    ram_data, ram_datain : std_logic_vector(15 downto 0);
  signal ram_adr : std_logic_vector(7 downto 0);

  signal red, green : std_logic_vector (2 downto 0);
  signal blue       : std_logic_vector (2 downto 1);
  signal xpos, ypos : std_logic_vector (9 downto 0);

  signal ppu_select : std_logic_vector(4 downto 0);
  signal ppu_we     : std_logic;
  signal ppu_done   : std_logic;
  signal ppu_data   : std_logic_vector(9 downto 0);
  signal ppu_datain : std_logic_vector(9 downto 0);

  signal posDataX, posDataY   : std_logic_vector(9 downto 0);
  signal posDataX2, posDataY2 : std_logic_vector(9 downto 0);

  --signal MISO, MOSI, SS, SCLK : std_logic;
  signal LED  : std_logic_vector(2 downto 0);
  signal LED2 : std_logic_vector(2 downto 0);

  signal ledvalue : std_logic_vector(15 downto 0);

begin

  cpu_inst : cpu port map (
    clk       => clk,
    rst       => rst,
    mem_write => cpu_w,
    mem_done  => cpu_done,
    mem_wait  => cpu_wait,
    pm_adr    => cpu_adr,
    pm_data   => cpu_data,
    pm_datain => cpu_datain
    );

  mmio_inst : mmio port map (
    lamp => lamp,
    clk        => clk,
    rst        => rst,
    -- CPU
    cpu_w      => cpu_w,
    cpu_r      => '1',
    cpu_adr    => cpu_adr,
    cpu_data   => cpu_data,
    cpu_datain => cpu_datain,
    cpu_wait   => cpu_wait,
    cpu_done   => cpu_done,
    -- Ram
    ram_we     => ram_we,
    ram_adr    => ram_adr,
    ram_data   => ram_data,
    ram_datain => ram_datain,
    -- PPU
    ppu_out    => ppu_data,
    ppu_in     => ppu_datain,
    ppu_select => ppu_select,
    ppu_we     => ppu_we,
    ppu_done   => ppu_done,
    -- JOYSTICK
    joy1x      => posDataX,
    joy1y      => posDataY,
    joy1k      => LED,
    joy2x      => posDataX2,
    joy2y      => posDataY2,
    joy2k      => LED2,
    ledout     => ledvalue
    );


  leddriver_inst : leddriver port map(
    clk      => clk,
    rst      => rst,
    ca       => ca,
    cb       => cb,
    cc       => cc,
    cd       => cd,
    ce       => ce,
    cf       => cf,
    cg       => cg,
    dp       => dp,
    an       => an,
    ledvalue => ledvalue
    );


  ram_inst : ram port map (
    clk           => clk,
    write_address => ram_adr,
    read_address  => ram_adr,
    we            => ram_we,
    q             => ram_data,
    data          => ram_datain
    );

  p : ppu port map (
    clk         => clk,
    rst         => rst,
    xpos        => xpos,
    ypos        => ypos,
    vgaRed      => red,
    vgaGreen    => green,
    vgaBlue     => blue,
    data_select => ppu_select,
    data_we     => ppu_we,
    data_done   => ppu_done,
    data_in     => ppu_data,
    data_out    => ppu_datain
    );

  vga : vga_circuit port map (
    xpos     => xpos,
    ypos     => ypos,
    red      => red,
    green    => green,
    blue     => blue,
    clk      => clk,
    rst      => rst,
    vgaRed   => vgaRed,
    vgaGreen => vgaGreen,
    vgaBlue  => vgaBlue,
    hsynch   => hsynch,
    vsynch   => vsynch
    );

  jstk : PmodJSTK_Demo port map (
    CLK      => clk,
    RST      => rst,
    MISO     => MISO,
    SW       => "100" ,
    SS       => SS,
    MOSI     => MOSI,
    SCLK     => SCLK,
    posDataX => posDataX,
    posDataY => posDataY,
    LED      => LED
    );

  jstk2 : PmodJSTK_Demo port map (
    CLK      => clk,
    RST      => rst,
    MISO     => MISO2,
    SW       => "010",
    SS       => SS2,
    MOSI     => MOSI2,
    SCLK     => SCLK2,
    posDataX => posDataX2,
    posDataY => posDataY2,
    LED      => LED2
    );


end rtl;
