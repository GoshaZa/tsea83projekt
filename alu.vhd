library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use work.cpu_types.all;

entity ALU is
  port (clk, rst   : in  std_logic;
        Z, N, C, V : out std_logic;
        a, b       : in  std_logic_vector(15 downto 0);
        styr       : in  std_logic_vector(3 downto 0);
        ut         : out std_logic_vector(15 downto 0));
end ALU;

architecture Behavioral of ALU is

  signal temp     : std_logic_vector(16 downto 0);
  signal V_tmp    : std_logic;

begin

  process(clk, rst)

  begin
    if rst = '1' then
      temp <= "00000000000000000";
      Z    <= '0';
      C    <= '0';
      N    <= '0';
      V    <= '0';
    elsif rising_edge(clk) then
      case Styr is
        when alu_none =>                --
          null;
        when alu_add =>                 -- +
          if conv_integer(B) > 65535 - conv_integer(A) then
            V     <= '1';
            V_tmp <= '1';
          else
            V     <= '0';
            V_tmp <= '0';
          end if;
          temp(15 downto 0) <= A + B;
        when alu_sub =>                 -- -
          temp <= std_logic_vector(to_unsigned(conv_integer(A) - conv_integer(B), 17));
          if A = B then
            Z <= '1';
          else
            Z <= '0';
          end if;

          if B > A then
            V     <= '1';
            V_tmp <= '1';
            N     <= '1';
          else
            V     <= '0';
            V_tmp <= '0';
            N     <= '0';
          end if;
        when alu_and =>                 -- AND
          temp <= '0' & (A and B);
        when alu_or =>                  -- OR
          temp <= '0' & (A or B);
        when alu_lsl =>                 -- LSL
          temp(15 downto 1) <= A(14 downto 0);
          temp(0)           <= '0';
          if A(15) = '1' then
            temp(16) <= '1';
          end if;
        when alu_lsr =>                 -- LSR
          temp(14 downto 0) <= A(15 downto 1);
          temp(15)          <= '0';
          if A(0) = '1' then
            temp(16) <= '1';
          end if;
        when alu_fb =>                  -- FB
          temp(15 downto 0) <= B;
        when alu_not =>                 -- NOT
          temp <= '0' & not A;
        when alu_nand =>                -- NAND
          temp <= '0' & A nand B;
        when alu_nor =>                 -- NOR
          temp <= '0' & A nor B;
        when alu_add_noflag =>          -- ADD without setting flags
          temp(15 downto 0) <= A + B;
        when others =>                  -- Do nothing
          null;
      end case;
      if Styr /= alu_add_noflag then
        if temp(15 downto 0) = "0000000000000000" then
          if V_tmp = '0' then           -- Don't set Zero flag if Overflow
            Z <= '1';
          else
            Z <= '0';
          end if;
        end if;
        C <= temp(16);
      end if;
    end if;

  end process;
  UT <= temp(15 downto 0);

end Behavioral;
