library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity cpu_tb is
end cpu_tb;

architecture behavior of cpu_tb is

  -- Component Declaration for the Unit Under Test (UUT)
  
  component cpu
    port(
      clk       : in  std_logic;
      rst       : in  std_logic;
      mem_wait  : out std_logic;
      mem_write : out std_logic;
      mem_done  : in  std_logic;
      pm_adr    : out std_logic_vector(15 downto 0);
      pm_data   : out std_logic_vector(16 - 1 downto 0);
      pm_datain : in  std_logic_vector(16 - 1 downto 0)
      );
  end component;


  --Inputs
  signal clk      : std_logic := '0';
  signal rst      : std_logic := '0';
  signal mem_done : std_logic := '1';

  --Outputs
  signal mem_wait  : std_logic;
  signal pm_adr    : std_logic_vector(15 downto 0);
  signal pm_data   : std_logic_vector(15 downto 0) := "0000" & "10" & "00" & "00000000";
  signal pm_datain : std_logic_vector(15 downto 0) := "0000" & "10" & "00" & "00000000";

  -- Clock period definitions
  constant clk_period : time := 10 ns;
  
begin

  -- Instantiate the Unit Under Test (UUT)
  uut : cpu port map (
    clk       => clk,
    rst       => rst,
    mem_wait  => mem_wait,
    mem_done  => mem_done,
    pm_adr    => pm_adr,
    pm_data   => pm_data,
    pm_datain => pm_datain
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    rst <= '1';

    wait for 100 ns;
    rst <= '0';

    wait until mem_wait = '1';
    pm_datain <= "0001" & "11" & "01" & "00000000";  -- LOAD

    mem_done <= '0';
    wait for clk_period*3;
    mem_done <= '1';

    wait until mem_wait = '1';
    pm_datain <= "0000001000111111";

    wait until mem_wait = '1';
    pm_datain <= "0011" & "11" & "00" & "00000000";  -- ADD

    mem_done <= '0';
    wait for clk_period*5;
    mem_done <= '1';

    wait until mem_wait = '1';
    wait until mem_wait = '1';
    pm_datain <= "0100" & "11" & "00" & "00000000";  -- SUB

    wait until mem_wait = '1';
    wait until mem_wait = '1';
    pm_datain <= "0010" & "11" & "00" & "00000000";  -- STORE

    wait until mem_wait = '1';
    wait until mem_wait = '1';
    pm_datain <= "0000" & "00" & "00" & "00000000";  -- HALT

    wait;
  end process;

end;
