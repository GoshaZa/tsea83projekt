library ieee;
use ieee.std_logic_1164.all;

entity stack_reg_tb is
end stack_reg_tb;

architecture behavior of stack_reg_tb is
  component stack_register
    port(
      clk        : in  std_logic;
      rst        : in  std_logic;
      r          : in  std_logic;
      w          : in  std_logic;
      async_data : out std_logic_vector(15 downto 0);
      data       : out std_logic_vector(15 downto 0);
      datain     : in  std_logic_vector(15 downto 0)
      );
  end component;


  --Inputs
  signal clk : std_logic := '0';
  signal rst : std_logic := '0';
  signal pop : std_logic := '0';
  signal w   : std_logic := '0';

  signal datain : std_logic_vector(15 downto 0) := (others => '0');

  --Outputs
  signal async_data : std_logic_vector(15 downto 0);
  signal data       : std_logic_vector(15 downto 0);

  -- Clock period definitions
  constant clk_period : time := 10 ns;

begin

  -- Instantiate the Unit Under Test (UUT)
  uut : stack_register port map (
    clk        => clk,
    rst        => rst,
    r          => pop,
    w          => w,
    async_data => async_data,
    data       => data,
    datain     => datain
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    rst <= '1';
    wait for 100 ns;
    rst <= '0';

    wait for clk_period*10;
    w      <= '1';
    datain <= X"0001";
    wait for clk_period;
    datain <= X"0002";
    wait for clk_period;
    w      <= '0';
    wait for clk_period*2;
    pop    <= '1';
    -- insert stimulus here

    wait;
  end process;

end;
