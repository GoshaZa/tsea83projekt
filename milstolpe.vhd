library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.numeric_std.all;

entity milstolpe is

  port (
    clk, rst   : in std_logic;
    btnd, btnr : in std_logic;

    hsynch, vsynch   : out std_logic;
    vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
    vgaBlue          : out std_logic_vector (2 downto 1);

    MISO, MISO2 : in  std_logic;        -- Master In Slave Out, JA3
    SW          : in  std_logic_vector (2 downto 0);  -- Switches 2, 1, and 0
    SS, SS2     : out std_logic;        -- Slave Select, Pin 1, Port JA
    MOSI, MOSI2 : out std_logic;        -- Master Out Slave In, Pin 2, Port JA
    SCLK, SCLK2 : out std_logic         -- Serial Clock, Pin 4, Port JA
    );

end milstolpe;

architecture Behavioral of milstolpe is

  component PmodJSTK_Demo
    port (CLK      : in  std_logic;     -- 100Mhz onboard clock
          RST      : in  std_logic;     -- Button D
          MISO     : in  std_logic;     -- Master In Slave Out, JA3
          SW       : in  std_logic_vector (2 downto 0);  -- Switches 2, 1, and 0
          SS       : out std_logic;     -- Slave Select, Pin 1, Port JA
          MOSI     : out std_logic;     -- Master Out Slave In, Pin 2, Port JA
          SCLK     : out std_logic;     -- Serial Clock, Pin 4, Port JA
          LED      : out std_logic_vector (2 downto 0);  -- LEDs 2, 1, and 0
          posDataX : out std_logic_vector(9 downto 0);
          posDataY : out std_logic_vector(9 downto 0));
  end component;

  component ppu
    port (clk, rst : in std_logic;

          ypos : in std_logic_vector (9 downto 0);
          xpos : in std_logic_vector (9 downto 0);

          vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
          vgaBlue          : out std_logic_vector (2 downto 1);

          x1, y1, x2, y2 : in std_logic_vector(9 downto 0);
          rot1, rot2     : in std_logic_vector(2 downto 0));
  end component;

  component vga_circuit
    port (clk, rst   : in std_logic;
          red, green : in std_logic_vector (2 downto 0);
          blue       : in std_logic_vector (1 downto 0);

          hsynch, vsynch   : out std_logic;
          vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
          vgaBlue          : out std_logic_vector (2 downto 1);
          ypos             : out std_logic_vector (9 downto 0);
          xpos             : out std_logic_vector (9 downto 0);

          -- Test bench
          hcnt_out           : out std_logic_vector (9 downto 0);
          vcnt_out           : out std_logic_vector (9 downto 0);
          video_on_out,
          h_on_out, v_on_out : out std_logic);
  end component;

  signal red, green : std_logic_vector (2 downto 0);
  signal blue       : std_logic_vector (2 downto 1);
  signal xpos, ypos : std_logic_vector (9 downto 0);

  signal x1, y1, xx2, yy2 : std_logic_vector(9 downto 0);
  signal rot1, rot2       : std_logic_vector (2 downto 0);

  signal posDataX, posDataY   : std_logic_vector(9 downto 0);
  signal posDataX2, posDataY2 : std_logic_vector(9 downto 0);

  --signal MISO, MOSI, SS, SCLK : std_logic;
  signal LED  : std_logic_vector(2 downto 0);
  signal LED2 : std_logic_vector(2 downto 0);

begin  -- Behavioral

  p : ppu port map (
    clk      => clk,
    rst      => rst,
    xpos     => xpos,
    ypos     => ypos,
    vgaRed   => red,
    vgaGreen => green,
    vgaBlue  => blue,
    x1       => x1,
    y1       => y1,
    rot1     => rot1,
    x2       => xx2,
    y2       => yy2,
    rot2     => rot2);

  vga : vga_circuit port map (
    xpos     => xpos,
    ypos     => ypos,
    red      => red,
    green    => green,
    blue     => blue,
    clk      => clk,
    rst      => rst,
    vgaRed   => vgaRed,
    vgaGreen => vgaGreen,
    vgaBlue  => vgaBlue,
    hsynch   => hsynch,
    vsynch   => vsynch);

  jstk : PmodJSTK_Demo port map (
    CLK      => clk,
    RST      => rst,
    MISO     => MISO,
    SW       => SW,
    SS       => SS,
    MOSI     => MOSI,
    SCLK     => SCLK,
    posDataX => posDataX,
    posDataY => posDataY,
    LED      => LED);

  jstk2 : PmodJSTK_Demo port map (
    CLK      => clk,
    RST      => rst,
    MISO     => MISO2,
    SW       => SW,
    SS       => SS2,
    MOSI     => MOSI2,
    SCLK     => SCLK2,
    posDataX => posDataX2,
    posDataY => posDataY2,
    LED      => LED2);


  process (clk)
    variable x : integer := 80;
    variable y : integer := 80;

    variable x2 : integer := 80;
    variable y2 : integer := 80;


    variable cnt : integer := 0;

    variable rot  : std_logic_vector(2 downto 0) := "000";
    variable rot2 : std_logic_vector(2 downto 0) := "000";
  begin  -- process clk
    if rising_edge(clk) then
      if cnt = 0 then

        if posDataX > 600 then
          x := x + (conv_integer(posDataX) - 500) / 128;
        end if;
        if posDataX < 400 then
          x := x - (500 - conv_integer(posDataX)) / 128;
        end if;

        if posDataY > 600 then
          y := y - (conv_integer(posDataY) - 500) / 128;
        end if;
        if posDataY < 400 then
          y := y + (500 - conv_integer(posDataY)) / 128;
        end if;

        x1 <= std_logic_vector(to_unsigned(x, 10));
        y1 <= std_logic_vector(to_unsigned(y, 10));
-------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-------------------------------------------------------------------------------
        if posDataX2 > 600 then
          x2 := x2 + (conv_integer(posDataX2) - 500) / 128;
        end if;
        if posDataX2 < 400 then
          x2 := x2 - (500 - conv_integer(posDataX2)) / 128;
        end if;

        if posDataY2 > 600 then
          y2 := y2 - (conv_integer(posDataY2) - 500) / 128;
        end if;
        if posDataY2 < 400 then
          y2 := y2 + (500 - conv_integer(posDataY2)) / 128;
        end if;

        xx2 <= std_logic_vector(to_unsigned(x2, 10));
        yy2 <= std_logic_vector(to_unsigned(y2, 10));

      end if;

      cnt := cnt + 1;

      if cnt = 1400000 then
        cnt := 0;
      end if;
    end if;
  end process;

  process (clk)
    variable cnt : integer := 0;
  begin  -- process
    if rising_edge(clk) then
      if cnt = 10000000 then
        cnt := 0;
        if led(1) = '1' then
          rot1 <= rot1 + 1;
        elsif led(2) = '1' then
          rot1 <= rot1 - 1;
        end if;

        if led2(1) = '1' then
          rot2 <= rot2 + 1;
        elsif led2(2) = '1' then
          rot2 <= rot2 - 1;
        end if;
      end if;
      cnt := cnt + 1;
    end if;
  end process;

end Behavioral;
