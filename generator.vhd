library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.STD_LOGIC_UNSIGNED.all;

use IEEE.numeric_std.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity generator is
  port (clk, rst   : in std_logic;
        btnd, btnr : in std_logic;

        hsynch, vsynch   : out std_logic;
        vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
        vgaBlue          : out std_logic_vector (2 downto 1);
        led              : out std_logic_vector (7 downto 0));
end generator;

architecture generate_pixel of generator is
  component vga_circuit
    port (clk, rst   : in std_logic;
          red, green : in std_logic_vector (2 downto 0);
          blue       : in std_logic_vector (1 downto 0);

          hsynch, vsynch   : out std_logic;
          vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
          vgaBlue          : out std_logic_vector (2 downto 1);
          ypos             : out std_logic_vector (9 downto 0);
          xpos             : out std_logic_vector (9 downto 0);

          -- Test bench
          hcnt_out           : out std_logic_vector (9 downto 0);
          vcnt_out           : out std_logic_vector (9 downto 0);
          video_on_out,
          h_on_out, v_on_out : out std_logic);
  end component;

  component ppu is
    port (clk, rst : in std_logic;

          ypos : in std_logic_vector (9 downto 0);
          xpos : in std_logic_vector (9 downto 0);

          vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
          vgaBlue          : out std_logic_vector (2 downto 1));
  end component;

  signal red, green : std_logic_vector (2 downto 0);
  signal blue       : std_logic_vector (2 downto 1);
  signal xpos, ypos : std_logic_vector (9 downto 0);

begin

  p : ppu port map (
    clk      => clk,
    rst      => rst,
    xpos     => xpos,
    ypos     => ypos,
    vgaRed   => red,
    vgaGreen => green,
    vgaBlue  => blue);

  vga : vga_circuit port map (
    xpos     => xpos,
    ypos     => ypos,
    red      => red,
    green    => green,
    blue     => blue,
    clk      => clk,
    rst      => rst,
    vgaRed   => vgaRed,
    vgaGreen => vgaGreen,
    vgaBlue  => vgaBlue,
    hsynch   => hsynch,
    vsynch   => vsynch);

end architecture generate_pixel;
