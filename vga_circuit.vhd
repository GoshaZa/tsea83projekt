library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.STD_LOGIC_UNSIGNED.all;

entity vga_circuit is
  port (clk, rst   : in std_logic;
        red, green : in std_logic_vector (2 downto 0);
        blue       : in std_logic_vector (1 downto 0);

        hsynch, vsynch   : out std_logic;
        vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
        vgaBlue          : out std_logic_vector (2 downto 1);
        ypos             : out std_logic_vector (9 downto 0);
        xpos             : out std_logic_vector (9 downto 0);

        -- Test bench
        hcnt_out           : out std_logic_vector (9 downto 0);
        vcnt_out           : out std_logic_vector (9 downto 0);
        video_on_out,
        h_on_out, v_on_out : out std_logic);
end vga_circuit;

architecture Behavioral of vga_circuit is

  signal hcnt                 : std_logic_vector (9 downto 0);  -- 0 - 799
  signal vcnt                 : std_logic_vector (9 downto 0);  -- 0 - 520
  signal pixel_clk            : std_logic_vector (1 downto 0) := "00";
  signal h_on, v_on, video_on : std_logic                     := '0';

begin  -- architecture Behavioral

  -- purpose: Counts the pixel_clk up 0 - 4, obtaining 25MHz on pixel_clk = 0
  -- inputs : clk, rst
  -- outputs: 
  mod_px_clk : process (clk, rst) is
  begin  -- process mod_px_clk
    if rst = '1' then
      pixel_clk <= "00";
    elsif rising_edge(clk) then  -- rising clock edge
      pixel_clk <= pixel_clk + 1;
    end if;
  end process mod_px_clk;

  -- purpose: Count up horizontally (inc columns "outside")
  -- inputs : clk, rst
  hcnt_p : process (clk, rst) is
  begin  -- process hcnt_p
    if rst = '1' then
      hcnt   <= (others => '0');
      hsynch <= '0';
      h_on   <= '0';
    elsif rising_edge(clk) and pixel_clk = 0 then
      if hcnt = 799 then
        hcnt   <= (others => '0');
        hsynch <= '0';
      else
        hcnt <= hcnt + 1;
      end if;

      if (hcnt = 95) then
        hsynch <= '1';
      end if;

      if hcnt = 143 then
        h_on <= '1';
      end if;
      if hcnt = 783 then
        h_on <= '0';
      end if;
    end if;
  end process hcnt_p;
  hcnt_out <= hcnt;
  h_on_out <= h_on;

  -- purpose: Count up vertically (inc rows "outside")
  -- inputs : clk, rst
  vcnt_p : process (clk, rst) is
  begin  -- process vcnt_p
    if rst = '1' then
      vcnt   <= (others => '0');
      vsynch <= '0';
      v_on   <= '0';
    elsif rising_edge(clk) and hcnt = 799 and pixel_clk = 0 then
      if vcnt = 520 then
        vcnt   <= (others => '0');
        vsynch <= '0';
      else
        vcnt <= vcnt + 1;
      end if;

      if (vcnt = 1) then
        vsynch <= '1';
      end if;

      if vcnt = 30 then
        v_on <= '1';
      end if;

      if vcnt = 510 then
        v_on <= '0';
      end if;
    end if;
  end process vcnt_p;
  vcnt_out <= vcnt;
  v_on_out <= v_on;

  video_on <= v_on and h_on;

  video_on_out <= video_on;


  -- purpose: Convert internal hcnt and vcnt to "real" x and y positions
  -- inputs : clk, rst
  conv_xy : process (clk, rst) is
  begin  -- process hcnt_p
    if rst = '1' then
      xpos <= (others => '0');
      ypos <= (others => '0');
    elsif rising_edge(clk) and pixel_clk = 0 then
      if (hcnt < 144) or (hcnt > 783) then
        xpos <= (others => '0');
      else
        xpos <= hcnt - 144;
      end if;
      
      if (vcnt < 31) or (vcnt > 510) then
        ypos <= (others => '0');
      else
        ypos <= vcnt - 31;
      end if;
    end if;
  end process conv_xy;


  -- purpose: Only output colors when video is on and clocked
  -- inputs : clk, rst
  process (clk, rst) is
  begin  -- process hcnt_p
    if rst = '1' then
      vgaRed   <= "000";
      vgaGreen <= "000";
      vgaBlue  <= "00";
    elsif rising_edge(clk) and pixel_clk = 0 then
      if video_on = '1' then
        vgaRed   <= red;
        vgaGreen <= green;
        vgaBlue  <= blue;
      else
        vgaRed   <= "000";
        vgaGreen <= "000";
        vgaBlue  <= "00";
      end if;
    end if;
  end process;
  
end architecture Behavioral;
