library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity register_file is
  port (
    clk   : in  std_logic;
    w     : in  std_logic;
    q     : out std_logic_vector (15 downto 0);
    qin   : in  std_logic_vector (15 downto 0);
    reset : in  std_logic;
    grx   : in  std_logic_vector (1 downto 0));
end register_file;

architecture Behavioral of register_file is
  signal gr1, gr2, gr3, gr4 : std_logic_vector (15 downto 0) := "0000000000000000";

begin

  process(clk,reset) is
  begin
    if reset = '1' then
      gr1 <= "0000000000000000";
      gr2 <= "0000000000000000";
      gr3 <= "0000000000000000";
      gr4 <= "0000000000000000";
    elsif rising_edge(clk) then
      if w = '1' then
        if grx = "00" then
          gr1 <= qin;
        elsif grx = "01" then
          gr2 <= qin;
        elsif grx = "10" then
          gr3 <= qin;
        else
          gr4 <= qin;
        end if;
      end if;
    end if;
  end process;

  with grx select
    q <=
    gr1 when "00",
    gr2 when "01",
    gr3 when "10",
    gr4 when "11",
    gr1 when others;

end Behavioral;
