library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.cpu_types.all;

entity mikrostyr is
  
  port (
    clk, rst : in  std_logic;
    hold     : in  std_logic;
    address  : out std_logic_vector(5 downto 0);
    seq      : in  std_logic_vector(3 downto 0);
    madr     : in  std_logic_vector(5 downto 0);
    alu_f    : in  std_logic_vector(3 downto 0);
    ir       : in  std_logic_vector(15 downto 0));

end mikrostyr;

architecture rtl of mikrostyr is

  signal mPC    : std_logic_vector(5 downto 0);
  signal mSPC   : std_logic_vector(5 downto 0);
  alias M_part  : std_logic_vector(1 downto 0) is ir(9 downto 8);
  alias OP_part : std_logic_vector(3 downto 0) is ir(15 downto 12);

  signal halt : std_logic := '0';
begin  -- rtl

  process(clk, rst) is
  begin
    if rst = '1' then
      mPC  <= "000000";
      mSPC <= "000000";
      halt <= '0';
    elsif rising_edge(clk) and halt = '0' then
      case hold is
        when '0' =>
          case seq is
            when seq_pcpp =>                     -- mPC++
              mPC <= mPC + 1;
              report "mPC++" severity note;
            when seq_pck2 =>                     -- mPC = K2
              report "mPC=K2" severity note;
              case M_part is
                when "00"   => mPC <= "000011";
                when "01"   => mPC <= "000100";
                when "10"   => mPC <= "000101";
                when "11"   => mPC <= "000111";
                when others => null;
              end case;
            when seq_pck1 =>                     -- mPC = K1
              report "mPC=K1" severity note;
              case OP_part is
                when "0000" => mPC <= "001010";  -- Halt
                when "0001" => mPC <= "001011";  -- Load
                when "0010" => mPC <= "001100";  -- Store
                when "0011" => mPC <= "001101";  -- Add
                when "0100" => mPC <= "010001";  -- Sub
                when "0101" => mPC <= "010101";  -- CMP
                when "0110" => mPC <= "011000";  -- BRA
                when "0111" => mPC <= "011001";  -- BNE
                when "1000" => mPC <= "011011";  -- BEQ
                when "1001" => mPC <= "011101";  -- BGE
                when "1010" => mPC <= "011111";  -- BLE
                when "1011" => mPC <= "100010";  -- JSR
                when "1100" => mPC <= "100011";  -- RTS
                when others => null;
              end case;
            when seq_pc0 =>                      -- mPC=0
              report "mPC=0" severity note;
              mPC <= "000000";
            when seq_pcadr =>                    -- mPC = mADR
              mPC <= mADR;
            when seq_pcjsr =>                    -- mPC JSR mADR
              mSPC <= mPC + 1;
              mPC  <= mADR;
            when seq_pcrts =>                    -- mPC RTS
              mPC <= mSPC;
            when seq_jnz =>                      -- Jump if not Zero-flag
              if alu_f(3) = '0' then
                mPC <= mADR;
              else
                mPC <= mPC + 1;
              end if;
            when seq_jnn =>
              if alu_f(2) = '0' then
                mPC <= mADR;
              else
                mPC <= mPC + 1;
              end if;
            when seq_jn =>
              if alu_f(2) = '1' then
                mPC <= mADR;
              else
                mPC <= mPC + 1;
              end if;
            when seq_jz =>
              if alu_f(3) = '1' then
                mPC <= mADR;
              else
                mPC <= mPC + 1;
              end if;
            when seq_halt =>                     -- HALT
              halt <= '1';
              mPC  <= "000000";
            when others => null;
          end case;
        when others =>
          mPC <= mPC;
      end case;
    end if;
  end process;

  address <= mPC;
-- TODO:
-- JMP Z,N,C,O,L = 1
end rtl;
