# Makefile for hardware implementation on Xilinx FPGAs and ASICs
# Author: Andreas Ehliar <ehliar@isy.liu.se>
# 
# T is the testbench file for this project
# S is the synthesizable sources for this project
# U is the UCF file
# PART is the part

# Important makefile targets:
# make lab.sim	GUI simulation
# make lab.simc	batch simulation
# make lab.synth	Synthesize
# make lab.route	Route the design
# make lab.bitgen	Generate bit file
# make lab.timing	Generate timing report
# make lab.clean	Use whenever you change settings in the Makefile!
# make lab.prog	Downloads the bitfile to the FPGA. NOTE: Does not
#                       rebuild bitfile if source files have changed!
# make clean            Removes all generated files for all projects. Also
#                       backup files (*~) are removed.
# 
# VIKTIG NOTERING: Om du ändrar vilka filer som finns med i projektet så måste du köra
#                  make lab.clean
#
# Syntesrapporten ligger i lab-synthdir/xst/synth/design.syr
# Maprapporten (bra att kolla i för arearapportens skull) ligger i lab-synthdir/layoutdefault/design_map.mrp
# Timingrapporten (skapas av make lab.timing) ligger i lab-synthdir/layoutdefault/design.trw

# (Or proj2.simc, proj2.sim, etc, depending on the name of the
# project)

XILINX_INIT = source /opt/Xilinx/12.4/ISE_DS/settings64.sh;
PART=xc6slx16-3-csg324


lab.%: S=lab.vhd leddriver.vhd
lab.%: T=lab_tb.vhd
lab.%: U=lab.ucf


vga_circuit.%: S=vga_circuit.vhd
vga_circuit.%: T=lab_tb.vhd
vga_circuit.%: U=vga.ucf

generator.%: S=generator.vhd vga_circuit.vhd ppu.vhd bild_minne.vhd sprite_minne.vhd
generator.%: T=lab_tb.vhd
generator.%: U=vga.ucf

milstolpe.%: S=milstolpe.vhd vga_circuit.vhd ppu.vhd bild_minne.vhd sprite_minne.vhd ClkDiv_5Hz.vhd ClkDiv_66_67kHz.vhd PmodJSTK_Demo.vhd PmodJSTK.vhd spiCtrl.vhd spiMode0.vhd
milstolpe.%: T=lab_tb.vhd
milstolpe.%: U=milstolpe.ucf

top.%: S=top.vhd vga_circuit.vhd ppu.vhd bild_minne.vhd sprite_minne.vhd ClkDiv_5Hz.vhd ClkDiv_66_67kHz.vhd cpu.vhd alu.vhd counter.vhd gp_reg.vhd mikrominne.vhd mikrostyr.vhd mmio.vhd ram.vhd register_file.vhd PmodJSTK_Demo.vhd PmodJSTK.vhd spiCtrl.vhd spiMode0.vhd alu_container.vhd cpu_types.vhd alu_container.vhd leddriver.vhd stack_reg.vhd
top.%: T=top_tb.vhd
top.%: U=top.ucf

# Det här är ett exempel på hur man kan skriva en testbänk som är
# relevant, även om man kör en simulering i batchläge (make batchlab.simc)
batchlab.%: S=lab.vhd leddriver.vhd
batchlab.%: T=batchlab_tb.vhd tb_print7seg.vhd
batchlab.%: U=lab.ucf


# Misc functions that are good to have
include build/util.mk
# Setup simulation environment
include build/vsim.mk
# Setup synthesis environment
include build/xst.mk
# Setup backend flow environment
include build/xilinx-par.mk
# Setup tools for programming the FPGA
include build/digilentprog.mk



# Alternative synthesis methods
# The following is for ASIC synthesis
#include design_compiler.mk
# The following is for synthesis to a Xilinx target using Precision.
#include precision-xilinx.mk



