
library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use ieee.numeric_std.all;

use work.cpu_types.all;

entity alu_container_tb is
end alu_container_tb;

architecture behavior of alu_container_tb is

  -- Component Declaration for the Unit Under Test (UUT)

  component alu_container
    port(
      clk      : in  std_logic;
      rst      : in  std_logic;
      styr     : in  std_logic_vector(3 downto 0);
      from_bus : in  std_logic_vector(15 downto 0);
      r        : in  std_logic;
      flags    : out std_logic_vector(3 downto 0);
      data_out : out std_logic_vector(15 downto 0)
      );
  end component;


  --Inputs
  signal clk      : std_logic                     := '0';
  signal rst      : std_logic                     := '0';
  signal styr     : std_logic_vector(3 downto 0)  := (others => '0');
  signal from_bus : std_logic_vector(15 downto 0) := (others => '0');
  signal r        : std_logic                     := '0';

  --Outputs
  signal flags    : std_logic_vector(3 downto 0);
  signal data_out : std_logic_vector(15 downto 0);

  -- Clock period definitions
  constant clk_period : time := 10 ns;


  function to_string(sv : std_logic_vector) return string is
    use Std.TextIO.all;
    variable bv : bit_vector(sv'range) := to_bitvector(sv);
    variable lp : line;
  begin
    write(lp, bv);
    return lp.all;
  end;
begin

  -- Instantiate the Unit Under Test (UUT)
  uut : alu_container port map (
    clk      => clk,
    rst      => rst,
    styr     => styr,
    from_bus => from_bus,
    r        => r,
    flags    => flags,
    data_out => data_out
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    r   <= '1';

    wait for clk_period*10;

    -- Lägg 1 på buss
    wait until rising_edge(clk);
    from_bus <= "0000000000000001";
    styr     <= alu_fb;

    -- Shift left
    wait until rising_edge(clk);
    from_bus <= "0000000000000001";
    styr     <= alu_lsl;

    report to_string(data_out) severity note;


    -- Plus 1
    wait until rising_edge(clk);
    styr <= alu_add;

    -- And
    wait until rising_edge(clk);
    styr <= alu_and;

    -- Minus 1
    wait until rising_edge(clk);
    styr <= alu_sub;

    -- Inget
    wait until rising_edge(clk);
    styr <= alu_none;

    wait;
  end process;

end;
