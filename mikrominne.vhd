library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cpu_types.all;

entity mikrominne is
  generic
    (ADDRESS_WIDTH : integer := 6;
     DATA_WIDTH    : integer := 24);
  port
    (read_address : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
     data         : out std_logic_vector(DATA_WIDTH - 1 downto 0));
end entity;

architecture rtl of mikrominne is
  type     RAM is array(0 to 63) of std_logic_vector(DATA_WIDTH - 1 downto 0);
  constant all_zero   : std_logic_vector(DATA_WIDTH-1 downto 0) := "000000000000000000000000";
  constant alu_none   : std_logic_vector(3 downto 0)            := "0000";
  constant nobus      : std_logic_vector(2 downto 0)            := "000";
  constant spzero     : std_logic_vector(1 downto 0)            := "00";
  constant lczero     : std_logic_vector(1 downto 0)            := "00";
  constant seqnone    : std_logic_vector(3 downto 0)            := "0000";
  constant myadr_zero : std_logic_vector(5 downto 0)            := "000000";


  signal ram_block : RAM := (
    -- ALU TB FB S P LC SEQ mADR
    -- 00 -- ASR := PC
    alu_none & tb_pc & fb_asr & spzero & lczero & seq_pcpp & myadr_zero,
    -- 01 -- IR := PM, PC++
    alu_none & tb_pm & fb_ir & "01" & lczero & seq_pcpp & myadr_zero,
    -- 02 -- uPC := K2
    alu_none & nobus & nobus & spzero & lczero & seq_pck2 & myadr_zero,
    -- 03 -- ASR := IR, uPC := K1       ; Direkt
    alu_none & tb_ir & fb_asr & spzero & lczero & seq_pck1 & myadr_zero,
    -- 04 -- ASR := PC, PC++, uPC := K1 ; Immediate
    alu_none & tb_pc & fb_asr & "01" & lczero & seq_pck1 & myadr_zero,
    -- 05 -- ASR := IR                  ; Indirekt
    alu_none & tb_ir & fb_asr & spzero & lczero & seq_pcpp & myadr_zero,
    -- 06 -- ASR := PM, uPC := K1
    alu_none & tb_pm & fb_asr & spzero & lczero & seq_pck1 & myadr_zero,
    -- 07 -- AR := IR                   ; Indexerad
    alu_fb & tb_ir & fb_nop & spzero & lczero & seq_pcpp & myadr_zero,
    -- 08 -- AR := GR3 + AR
    alu_add_noflag & tb_gr & fb_nop & "10" & lczero & seq_pcpp & myadr_zero,
    -- 09 -- ASR := AR, uPC := K1
    alu_none & tb_ar & fb_asr & spzero & lczero & seq_pck1 & myadr_zero,
    -- 10 -- Halt
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_halt & myadr_zero,
    -- 11 -- GRx := PM, uPC := 0        ; Load
    alu_none & tb_pm & fb_gr & spzero & lczero & seq_pc0 & myadr_zero,
    -- 12 -- PM := GRx, uPC := 0        ; Store
    alu_none & tb_gr & fb_pm & spzero & lczero & seq_pc0 & myadr_zero,
    -- 13 -- AR := GR; uPC++           ; Add
    alu_fb & tb_gr & fb_nop & spzero & lczero & seq_pcpp & myadr_zero,
    -- 14 -- AR := GR + PM; uPC++
    alu_add & tb_pm & fb_nop & spzero & lczero & seq_pcpp & myadr_zero,
    -- 15 -- GRx := AR, uPC := O
    alu_none & tb_ar & fb_gr & spzero & lczero & seq_pc0 & myadr_zero,
    -- 16 --
    all_zero,
    -- 17 -- AR := PM; uPC++           ; Sub
    alu_fb & tb_gr & fb_nop & spzero & lczero & seq_pcpp & myadr_zero,
    -- 18 -- AR := GR(alu) - PM; uPC++
    alu_sub & tb_pm & fb_nop & spzero & lczero & seq_pcpp & myadr_zero,
    -- 19 -- GRx := AR, uPC := 0
    alu_none & tb_ar & fb_gr & spzero & lczero & seq_pc0 & myadr_zero,
    -- 20
    all_zero,
    -- 21 -- AR := GR; uPC++          ; CMP
    alu_fb & tb_gr & fb_nop & spzero & lczero & seq_pcpp & myadr_zero,
    -- 22 -- AR := GR - PM; uPC=0
    alu_sub & tb_pm & fb_nop & spzero & lczero & seq_pc0 & myadr_zero,
    -- 23 --
    all_zero,
    -- 24 -- PC := IR; uPC=0          ; BRA
    alu_none & tb_ir & fb_pc & spzero & lczero & seq_pc0 & myadr_zero,
    -- 25 --                          ; BNE
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_jnz & "011000",
    -- 26 --
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_pc0 & myadr_zero,
    -- 27                             ; BEQ
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_jz & "011000",
    -- 28 --
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_pc0 & myadr_zero,
    -- 29 --                          ; BGE
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_jnn & "011000", -- 24
    -- 30
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_pc0 & myadr_zero,
    -- 31 --                          ; BLE
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_jnn & "011011", -- BEQ 27
    -- 32
    alu_none & tb_nop & fb_nop & spzero & lczero & seq_pcadr & "011000",
    -- 33
    all_zero,
    -- 34 -- HR := PC, goto BRA       ; JSR
    alu_none & tb_pc & fb_hr & spzero & lczero & seq_pcadr & "011000",
    -- 35 -- PC := HR, uPC := 0       ; RTS
    alu_none & tb_hr & fb_pc & spzero & lczero & seq_pc0 & myadr_zero,
    -- 36
    all_zero,
    -- 37
    all_zero,
    -- 38
    all_zero,
    -- 39
    all_zero,
    -- 40
    all_zero,
    -- 41
    all_zero,
    -- 42
    all_zero,
    -- 43
    all_zero,
    -- 44
    all_zero,
    -- 45
    all_zero,
    -- 46
    all_zero,
    -- 47
    all_zero,
    -- 48
    all_zero,
    -- 49
    all_zero,
    -- 50
    all_zero,
    -- 51
    all_zero,
    -- 52
    all_zero,
    -- 53
    all_zero,
    -- 54
    all_zero,
    -- 55
    all_zero,
    -- 56
    all_zero,
    -- 57
    all_zero,
    -- 58
    all_zero,
    -- 59
    all_zero,
    -- 60
    all_zero,
    -- 61
    all_zero,
    -- 62
    all_zero,
    -- 63
    all_zero
    );
begin

  data <= ram_block(to_integer(unsigned(read_address)));

end rtl;
