library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu_tb is
end alu_tb;

architecture behavior of alu_tb is

  -- Component Declaration
  component ALU
    port(
      clk, rst : in std_logic;

      a, b : in std_logic_vector(15 downto 0);
      styr : in std_logic_vector(3 downto 0);

      Z, N, C, V : out std_logic;

      ut : out std_logic_vector(15 downto 0) bus
      );
  end component;

  signal clk : std_logic := '0';
  signal rst : std_logic := '0';
  signal rx  : std_logic := '1';

  signal a    : std_logic_vector(15 downto 0) := "1000000000000011";
  signal b    : std_logic_vector(15 downto 0) := "0000000000000001";
  signal styr : std_logic_vector(3 downto 0)  := "0101";
  signal ut   : std_logic_vector(15 downto 0);

  signal Z : std_logic;
  signal N : std_logic;
  signal C : std_logic;
  signal V : std_logic;


  signal tb_running : boolean := true;
-- alla bitar för 1234
begin

  -- Component Instantiation
  uut : ALU port map(
    clk => clk,
    rst => rst,

    Z => Z,
    N => N,
    C => C,
    V => V,

    a    => a,
    b    => b,
    styr => styr,
    ut   => ut);

  clk_gen : process
  begin
    while tb_running loop
      clk <= '0';
      wait for 5 ns;
      clk <= '1';
      wait for 5 ns;
    end loop;
    wait;
  end process;


  stimuli_generator : process
    variable i : integer;
  begin
    -- Aktivera reset ett litet tag.
    rst <= '1';
    wait for 100 ns;

    wait until rising_edge(clk);        -- se till att reset släpps synkront
                                        -- med klockan
    rst <= '0';
    report "Reset released" severity note;

    wait until rising_edge(clk);
    styr <= "0110";

    wait until rising_edge(clk);
    styr <= "0001";

    wait for 600ns;

    wait;
  end process;

end;
