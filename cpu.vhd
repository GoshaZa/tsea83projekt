library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.cpu_types.all;

entity cpu is
  port
    (clk, rst  : in  std_logic;
     mem_wait  : out std_logic;
     mem_write : out std_logic;
     mem_done  : in  std_logic;
     pm_adr    : out std_logic_vector(16 - 1 downto 0);
     pm_data   : out std_logic_vector(16 - 1 downto 0);
     pm_datain : in  std_logic_vector(16 - 1 downto 0));

end cpu;

architecture rtl of cpu is

  component general_purpose_register
    generic
      (width : integer := 16);
    port
      (clk, rst   : in  std_logic;
       w          : in  std_logic;
       async_data : out std_logic_vector(width-1 downto 0);
       data       : out std_logic_vector(width-1 downto 0);
       datain     : in  std_logic_vector(width-1 downto 0));
  end component;

  component stack_register
  generic
    (width  : integer := 16;
     height : integer := 4);
  port
    (clk, rst   : in  std_logic;
     r          : in  std_logic;
     w          : in  std_logic;
     async_data : out std_logic_vector(width-1 downto 0);
     data       : out std_logic_vector(width-1 downto 0);
     datain     : in  std_logic_vector(width-1 downto 0));
  end component;

  component mikrominne
    generic
      (ADDRESS_WIDTH : integer := 6;
       DATA_WIDTH    : integer := 24);
    port
      (read_address : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
       data         : out std_logic_vector(DATA_WIDTH - 1 downto 0));
  end component;

  component mikrostyr
    port (
      clk, rst : in  std_logic;
      hold     : in  std_logic;
      address  : out std_logic_vector(5 downto 0);
      seq      : in  std_logic_vector(3 downto 0);
      madr     : in  std_logic_vector(5 downto 0);
      alu_f    : in  std_logic_vector(3 downto 0);
      ir       : in  std_logic_vector(15 downto 0));
  end component;

  component alu_container
    port (
      clk, rst : in  std_logic;
      styr     : in  std_logic_vector(3 downto 0);
      from_bus : in  std_logic_vector(15 downto 0);
      flags    : out std_logic_vector(3 downto 0);
      data_out : out std_logic_vector(15 downto 0));
  end component;

  component counter
    port (clk   : in  std_logic;
          q     : out std_logic_vector(15 downto 0);
          qin   : in  std_logic_vector(15 downto 0);
          inc   : in  std_logic;
          reset : in  std_logic;
          w     : in  std_logic);
  end component;

  component register_file
    port (
      clk   : in  std_logic;
      w     : in  std_logic;
      q     : out std_logic_vector (15 downto 0);
      qin   : in  std_logic_vector (15 downto 0);
      reset : in  std_logic;
      grx   : in  std_logic_vector (1 downto 0));
  end component;

  constant mminne_bredd : integer := 24;
  constant bit_width    : integer := 16;

  signal hold     : std_logic;
  signal wait_mem : std_logic;

  signal styr_adr_ut : std_logic_vector(5 downto 0);
  signal mminne_ut   : std_logic_vector(mminne_bredd - 1 downto 0);
  alias mminne_seq   : std_logic_vector(3 downto 0) is mminne_ut(9 downto 6);
  alias mminne_madr  : std_logic_vector(5 downto 0) is mminne_ut(5 downto 0);
  alias mminne_alu   : std_logic_vector(3 downto 0) is mminne_ut(23 downto 20);
  alias mminne_tb    : std_logic_vector(2 downto 0) is mminne_ut(19 downto 17);
  alias mminne_fb    : std_logic_vector(2 downto 0) is mminne_ut(16 downto 14);
  alias mminne_s     : std_logic is mminne_ut(13);
  alias mminne_p     : std_logic is mminne_ut(12);
  alias mminne_lc    : std_logic_vector(1 downto 0) is mminne_ut(11 downto 10);
  signal alu_flags   : std_logic_vector(3 downto 0);

  signal ir_async  : std_logic_vector(bit_width-1 downto 0);
  signal asr_async : std_logic_vector(bit_width-1 downto 0);
  alias op_gr      : std_logic_vector(1 downto 0) is ir_async(11 downto 10);
  alias op_m       : std_logic_vector(1 downto 0) is ir_async(9 downto 8);

  signal tb      : std_logic_vector(6 downto 0);
  alias read_ir  : std_logic is tb(0);
  alias read_pm  : std_logic is tb(1);
  alias read_pc  : std_logic is tb(2);
  alias read_ar  : std_logic is tb(3);
  alias read_hr  : std_logic is tb(4);
  alias read_gr  : std_logic is tb(5);
  alias read_asr : std_logic is tb(6);

  signal fb       : std_logic_vector(6 downto 0);
  alias write_ir  : std_logic is fb(0);
  alias write_pm  : std_logic is fb(1);
  alias write_pc  : std_logic is fb(2);
  alias write_ar  : std_logic is fb(3);
  alias write_hr  : std_logic is fb(4);
  alias write_gr  : std_logic is fb(5);
  alias write_asr : std_logic is fb(6);

  signal pcpp : std_logic;

  signal alu_styr : std_logic_vector(3 downto 0);

  signal the_bus : std_logic_vector(bit_width-1 downto 0);

  signal which_gr : std_logic_vector(1 downto 0);


  signal alu_bus, gr_bus, hr_bus, asr_bus, ir_bus, pc_bus, ar_bus : std_logic_vector(15 downto 0);
begin

  mminne : mikrominne
    generic map
    (ADDRESS_WIDTH => 6, DATA_WIDTH => mminne_bredd)
    port map
    (read_address => styr_adr_ut,
     data         => mminne_ut);

  wait_mem <= read_pm or write_pm;
  hold     <= wait_mem and not mem_done;
  mem_wait <= wait_mem;

  pcpp <= mminne_p and not hold;

  with hold select
    alu_styr <=
    mminne_alu when '0',
    alu_none when others;

  mstyr : mikrostyr
    port map
    (clk     => clk,
     rst     => rst,
     hold    => hold,
     address => styr_adr_ut,
     madr    => mminne_madr,
     seq     => mminne_seq,
     alu_f   => alu_flags,
     ir      => ir_async);

  alu : alu_container
    port map
    (clk      => clk,
     rst      => rst,
     from_bus => the_bus,
     styr     => alu_styr,
     flags    => alu_flags,
     data_out => alu_bus);

  grx : register_file
    port map
    (clk   => clk,
     reset => rst,
     w     => write_gr,
     q     => gr_bus,
     qin   => the_bus,
     grx   => which_gr);

  with mminne_s select
    which_gr <=
    op_gr when '0',
    op_m  when '1',
    "00"  when others;

  hr : stack_register
    port map (
      clk    => clk,
      rst    => rst,
      r      => read_hr,
      w      => write_hr,
      datain => the_bus,
      data   => hr_bus);

  asr : general_purpose_register
    port map (
      clk        => clk,
      rst        => rst,
      w          => write_asr,
      async_data => asr_async,
      datain     => the_bus,
      data       => asr_bus);

  ir : general_purpose_register
    port map (
      clk        => clk,
      rst        => rst,
      w          => write_ir,
      async_data => ir_async,
      datain     => the_bus,
      data       => ir_bus);

  pc : counter
    port map (
      clk   => clk,
      reset => rst,
      inc   => pcpp,
      q     => pc_bus,
      qin   => the_bus,
      w     => write_pc);


  with mminne_tb select
    tb <=
    "0000000" when tb_nop,
    "0000001" when tb_ir,
    "0000010" when tb_pm,
    "0000100" when tb_pc,
    "0001000" when tb_ar,
    "0010000" when tb_hr,
    "0100000" when tb_gr,
    "1000000" when tb_asr,
    "0000000" when others;

  with mminne_fb select
    fb <=
    "0000000" when fb_nop,
    "0000001" when fb_ir,
    "0000010" when fb_pm,
    "0000100" when fb_pc,
    "0001000" when fb_ar,
    "0010000" when fb_hr,
    "0100000" when fb_gr,
    "1000000" when fb_asr,
    "0000000" when others;

  with mminne_tb select
    the_bus <=
    X"00" & ir_bus(7 downto 0) when tb_ir,
    pm_datain                  when tb_pm,
    pc_bus                     when tb_pc,
    alu_bus                    when tb_ar,
    hr_bus                     when tb_hr,
    gr_bus                     when tb_gr,
    asr_bus                    when tb_asr,
    (others => '0')            when others;

  pm_data <= the_bus;
  --process(read_pm, write_pm)
  --begin
  --  if write_pm = '1' then
  --    pm_data <= pm_bus;
  --  elsif read_pm = '1' then
  --    the_bus <= pm_data;
  --  else
  --    pm_data <= (others => 'Z');
  --  end if;
  --end process;

  mem_write <= write_pm;

  pm_adr <= asr_async;
end rtl;
