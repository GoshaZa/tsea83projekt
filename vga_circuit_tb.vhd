library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_circuit_tb is
end entity vga_circuit_tb;

architecture Behavior of vga_circuit_tb is
  component vga_circuit is
    port (clk, rst   : in std_logic;
          red, green : in std_logic_vector (2 downto 0);
          blue       : in std_logic_vector (1 downto 0);

          hsynch, vsynch   : out std_logic;
          vgaRed, vgaGreen : out std_logic_vector (2 downto 0);
          vgaBlue          : out std_logic_vector (2 downto 1);
          ypos             : out std_logic_vector (9 downto 0);
          xpos             : out std_logic_vector (9 downto 0);

          -- Test bench
          hcnt_out           : out std_logic_vector (9 downto 0);
          vcnt_out           : out std_logic_vector (9 downto 0);
          video_on_out,
          h_on_out, v_on_out : out std_logic);
  end component;

  signal clk, rst, hsynch, vsynch     : std_logic := '0';
  signal vgaRed, vgaGreen, red, green : std_logic_vector(2 downto 0);
  signal vgaBlue, blue                : std_logic_vector(2 downto 1);
  signal tb_running                   : boolean   := true;
  signal hcnt, xpos                   : std_logic_vector (9 downto 0);  -- 0 - 799
  signal vcnt, ypos                   : std_logic_vector (9 downto 0);  -- 0 - 799
  signal h_on, v_on, video_on         : std_logic := '0';
begin  -- architecture Behavioral

  uut : vga_circuit port map (
    xpos         => xpos,
    ypos         => ypos,
    h_on_out     => h_on,
    v_on_out     => v_on,
    video_on_out => video_on,
    hcnt_out     => hcnt,
    vcnt_out     => vcnt,
    red          => red,
    green        => green,
    blue         => blue,
    clk          => clk,
    rst          => rst,
    vgaRed       => vgaRed,
    vgaGreen     => vgaGreen,
    vgaBlue      => vgaBlue,
    hsynch       => hsynch,
    vsynch       => vsynch);

  -- 100 MHz system clock
  clk_gen : process
  begin
    while tb_running loop
      clk <= '0';
      wait for 5 ns;
      clk <= '1';
      wait for 5 ns;
    end loop;
    wait;
  end process;

  green <= xpos(9 downto 7);

  stimuli_generator : process
    variable i : integer;
  begin
    -- Aktivera reset ett litet tag.
    rst <= '1';
    wait for 500 ns;

    wait until rising_edge(clk);        -- se till att reset släpps synkront
                                        -- med klockan
    rst <= '0';
    report "Reset released" severity note;


    for i in 0 to 50000000 loop         -- Vänta ett antal klockcykler
      wait until rising_edge(clk);
    end loop;  -- i

    tb_running <= false;  -- Stanna klockan (vilket medför att inga
    -- nya event genereras vilket stannar
    -- simuleringen).
    wait;
  end process;

end architecture Behavior;
